#!/bin/bash

abort()
{
    echo $? >&2

    echo >&2 '***************'
    echo >&2 '*** ABORTED ***'
    echo >&2 '***************
'
    echo "An error occurred. Exiting..." >&2
    exit 1
}

# set up bash to call 'abort' function whenever a non-zero exit code is encountered
# * https://stackoverflow.com/a/22224317
# * https://vaneyckt.io/posts/safer_bash_scripts_with_set_euxo_pipefail/
trap 'abort' 0

set -Eeuo pipefail

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

#
# This repo may or may not have the relevant commit checked out in sidekiq_sw, if REPO_GIT_COMMIT is
# "committed", use whatever is committed in the submodule, otherwise "re-checkout" the submodule
# according to the REPO_GIT_COMMIT passed in from Jenkins
#
REPO_CHECKOUT=no
if [ x"$REPO_GIT_COMMIT" = x"committed" ]; then
    REPO_GIT_COMMIT=$(cd "$DIR/../sidekiq_sw"; git rev-parse --short HEAD)
else
    REPO_GIT_COMMIT=${REPO_GIT_COMMIT:0:12}
    REPO_CHECKOUT=yes
fi

if [ x"$REPO_GIT_REVISION" = x"committed" ]; then
    REPO_GIT_REVISION=$(cd "$DIR/../sidekiq_sw"; git name-rev --name-only "$REPO_GIT_COMMIT")
fi

if [ x"$REPO_CHECKOUT" = x"yes" ]; then
    (cd $DIR/../sidekiq_sw; \
     git submodule deinit -f --all; \
     git checkout $REPO_GIT_COMMIT; \
     git submodule sync --recursive; \
     git submodule update --init; \
     git submodule sync --recursive; \
     git submodule update --init --recursive)
fi

FN=sidekiq_sw/host_code/libsidekiq/sidekiq_core/inc/sidekiq_api.h
MJ=$(grep -m1 LIBSIDEKIQ_VERSION_MAJOR ${FN} | awk '{print $3}')
MN=$(grep -m1 LIBSIDEKIQ_VERSION_MINOR ${FN} | awk '{print $3}')
PT=$(grep -m1 LIBSIDEKIQ_VERSION_PATCH ${FN} | awk '{print $3}')
LB=$(grep -m2 LIBSIDEKIQ_VERSION_LABEL ${FN} | tail -n1 | awk '{print $3}')
DT=$(date +%Y%m%d)
GH=g$REPO_GIT_COMMIT

echo "GIT_REVISION is $REPO_GIT_REVISION" >&2
echo "GIT_COMMIT is $REPO_GIT_COMMIT" >&2

if [[ "$REPO_GIT_REVISION" =~ "tags/" ]]; then
    VER=$MJ.$MN.$PT
elif [[ "$REPO_GIT_REVISION" =~ "/release/" ]]; then
    # if the LABEL is empty (after discarding double quote) do not use a qualifier in $VER
    if [ -z "${LB//\"}" ]; then
        VER=$MJ.$MN.$PT
    else
        VER=$MJ.$MN.$PT-rc
    fi
else
    VER=$MJ.$MN.$PT-${BUILD_NUMBER:-manual}-$GH
fi

SRFS=$(readlink -f srfs)

echo "==================== BUILDING SDK ====================" >&2
(cd sdk_release; srfs_dir=$SRFS ./make_sdk_release.sh $VER)

echo "==================== BUILDING IMAGES ====================" >&2
(cd image_release; ./make_all.sh $DT $VER)

echo "==================== BUILDING INSTALLERS ====================" >&2
(cd system_release; ./make_all.sh $DT $VER)

echo "==================== COPYING ARTIFACTS ====================" >&2
mkdir -p components/{dma_driver,image,sdk} \
      components/system/{applications,installation,shared-libraries} \
      windows

################################################################################
# populate components/image
################################################################################
mv image_release/*.opk \
   image_release/sidekiq_image_*.tar.* \
   components/image

################################################################################
# populate components/system/applications
################################################################################
mv sdk_release/sidekiq_sdk_*prebuilt*.tar.* components/system/applications

################################################################################
# populate components/sdk
################################################################################
mv sdk_release/sidekiq_sdk_*.tar.xz components/sdk
(cd components/sdk; tar -xf sidekiq_sdk_v$VER.tar.xz --wildcards --show-transformed --xform='s|.*/||' "*Sidekiq_API_*.pdf" "*Sidekiq_Software_Development_Manual*.pdf" )

################################################################################
# extract top-level README.txt
################################################################################
tar -xf components/sdk/sidekiq_sdk_v$VER.tar.xz --wildcards --show-transformed --xform='s|.*/||' "*README.txt"

################################################################################
# populate components/system/shared-libraries
################################################################################
mv sdk_release/sidekiq-shared-libs* components/system/shared-libraries

################################################################################
# populate components/system/installation
################################################################################
mv system_release/install_sidekiq_*.sh \
   system_release/sidekiq_hardware_updater_*.sh .
(cd components/system/installation; \
 ls ../../../install_sidekiq_complete_*.sh \
    ../../../sidekiq_hardware_updater_*.sh \
    ../../../install_sidekiq_runtime_*.sh | xargs -r -n1 ln -snf)

################################################################################
# create tar to preserve symlinks
################################################################################
tar -cvpf artifacts.tar components/ install_sidekiq_*.sh sidekiq_hardware_updater_*.sh README.txt

# unregister the trap
trap - 0
