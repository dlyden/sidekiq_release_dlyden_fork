#!/bin/bash

abort()
{
    echo $? >&2

    echo >&2 '***************'
    echo >&2 '*** ABORTED ***'
    echo >&2 '***************
'
    echo "An error occurred. Exiting..." >&2
    exit 1
}

# set up bash to call 'abort' function whenever a non-zero exit code is encountered
# * https://stackoverflow.com/a/22224317
# * https://vaneyckt.io/posts/safer_bash_scripts_with_set_euxo_pipefail/
trap 'abort' 0

set -Eeuo pipefail

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

REPO_GIT_COMMIT=committed
REPO_GIT_REVISION=committed
export REPO_GIT_COMMIT REPO_GIT_REVISION

$DIR/build-sdk-jenkins-win64.sh

# unregister the trap
trap - 0
