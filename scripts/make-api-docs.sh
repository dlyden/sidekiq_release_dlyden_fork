#! /bin/bash
set -eo pipefail

abort()
{
    echo $? >&2

    echo >&2 '***************'
    echo >&2 '*** ABORTED ***'
    echo >&2 '***************
'
    echo "An error occurred. Exiting..." >&2
    exit 1
}

# set up bash to call 'abort' function whenever a non-zero exit code is encountered
# * https://stackoverflow.com/a/22224317
# * https://vaneyckt.io/posts/safer_bash_scripts_with_set_euxo_pipefail/
trap 'abort' 0

if [ $# -gt 1 ]; then
    echo "usage: $(basename $(readlink -f $0)) [Major.Minor.Patch]" >&2
    echo >&2
    echo "  version argument is optional, default is 'UNRELEASED'" >&2
    echo >&2
    exit 1
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DOC="$DIR/../api_doc"
CODE="$DIR/../sidekiq_sw/host_code"

# check for required tools

# epstopdf
# sudo apt install texlive-font-utils

# dot
# sudo apt install graphviz

# pdflatex
# sudo apt install --no-install-recommends texlive-latex-base texlive-latex-extra texlive-latex-recommended texlive-fonts-recommended texlive-fonts-extra

if [ $# -eq 0 ]; then
    VERSION=UNRELEASED
    echo "using 'UNRELEASED' as libsidekiq version string" >&2
    echo "if this is not desired behavior, pass an argument to this script instead" >&2
else
    VERSION=${VERSION:-$1}
    VERSION=$(cut -d. -f1-2 <<< "$VERSION").x
fi

current_year=$(date +"%Y")
sed -e "s,LIBSKIQ_VERSION,$VERSION,g" -e "s,LIBSKIQ_RELEASE_YEAR,${current_year},g" < "$DOC/header.tex.template" > "$DOC/header.tex"

USE_DOXYGEN_DOCKER=n
if hash docker 2>/dev/null; then
    if docker image inspect doxygen:latest >/dev/null 2>&1; then
        USE_DOXYGEN_DOCKER=y
    elif docker inspect doxygen:latest >/dev/null 2>&1; then
        # This could be an outdated syntax for the `docker` command
        USE_DOXYGEN_DOCKER=y
    fi
fi

case "$USE_DOXYGEN_DOCKER" in
    y)
        echo "=== Using Docker Doxygen..."
        # Remove any existing code directory (just to make it clear)
        if [ -d "$DOC/host_code" ]; then
            rmdir "$DOC/host_code"
        fi
        docker run --rm -u $(id -u):$(id -g) -v "$DOC":/data -v "$CODE":/data/host_code doxygen:latest doxygen Doxyfile
        ;;

    n)
        echo "=== Using local Doxygen..."
        (cd "$DOC"; ln -snf "$CODE" host_code; doxygen Doxyfile)
        ;;
esac

# copy logo over for use with the Desktop shortcut
cp "$DOC/images/epiq_logo_lite.svg" "$DOC/html/"

# make an ugly attempt at reordering
MKT=$(mktemp)
sed -n '/\\chapter{Sidekiq Transport Layer}/,/\\input{CustomTransport}/p' "$DOC/latex/refman.tex" >> $MKT
# It seems that sometimes the "Timestamp Slips" chapter is generated with an extra symbol in the
# title (e.g. "Timestamp Slips within A\+D9361 Products")... not sure why, but add in a wildcard
# here to ensure the match is made regardless if the extra symbol is there or not
sed -n '/\\chapter{Timestamp Slips within A.*D9361 Products}/,/\\input{AD9361TimestampSlip}/p' "$DOC/latex/refman.tex" >> $MKT
sed -n '/\\chapter{Class Index}/,/\\input{annotated}/p' "$DOC/latex/refman.tex" >> $MKT
sed -n '/\\chapter{Class Documentation}/,/\\input{structskiq__xport__tx__functions__t}/p' "$DOC/latex/refman.tex" >> $MKT

sed -i -e '/\\chapter{Sidekiq Transport Layer}/,/\\input{CustomTransport}/d' "$DOC/latex/refman.tex"
# See above section for information on the wildcard in this entry
sed -i -e '/\\chapter{Timestamp Slips within A.*D9361 Products}/,/\\input{AD9361TimestampSlip}/d' "$DOC/latex/refman.tex"
sed -i -e '/\\chapter{Class Index}/,/\\input{annotated}/d' "$DOC/latex/refman.tex"
sed -i -e '/\\chapter{Class Documentation}/,/\\input{structskiq__xport__tx__functions__t}/d' "$DOC/latex/refman.tex"

M=$(grep -m1 -n '%--- End generated contents ---' < "$DOC/latex/refman.tex" | cut -d: -f1)

OUTPUT=$(mktemp)
head -n $[M-1] "$DOC/latex/refman.tex" > $OUTPUT
cat $MKT >> $OUTPUT
tail -n +$M "$DOC/latex/refman.tex" >> $OUTPUT
mv -f $OUTPUT "$DOC/latex/refman.tex"

rm -f $MKT $OUTPUT

# fix up undefined control sequences that Doxygen creates on Ubuntu 16.04
sed -i -e 's,\\+,,g' "$DOC/latex"/*.tex

OS_TYPE=$(uname -o)
if [[ "$OS_TYPE" != "Msys" ]]; then
    (cd "$DOC/latex"; make pdf)
fi

echo >&2
echo -e "\tHTML output is in '$DOC/html'" >&2
if [[ "$OS_TYPE" != "Msys" ]]; then
    echo -e "\tPDF output is in '$DOC/latex/refman.pdf'" >&2
fi
echo >&2
echo -e "\tused '$VERSION' as libsidekiq version string" >&2
echo >&2

# unregister the trap
trap : 0
