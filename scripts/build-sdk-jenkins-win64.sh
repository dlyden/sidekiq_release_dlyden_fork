#!/bin/bash

abort()
{
    echo $? >&2

    echo >&2 '***************'
    echo >&2 '*** ABORTED ***'
    echo >&2 '***************
'
    echo "An error occurred. Exiting..." >&2
    exit 1
}

# set up bash to call 'abort' function whenever a non-zero exit code is encountered
# * https://stackoverflow.com/a/22224317
# * https://vaneyckt.io/posts/safer_bash_scripts_with_set_euxo_pipefail/
trap 'abort' 0

set -Eeuo pipefail

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

#
# This repo may or may not have the relevant commit checked out in sidekiq_sw, if REPO_GIT_COMMIT is
# "committed", use whatever is committed in the submodule, otherwise "re-checkout" the submodule
# according to the REPO_GIT_COMMIT passed in from Jenkins
#
(cd "$DIR/../"; git submodule update --init sidekiq_sw)

REPO_CHECKOUT=no
if [ x"$REPO_GIT_COMMIT" = x"committed" ]; then
    REPO_GIT_COMMIT=$(cd "$DIR/../sidekiq_sw"; git rev-parse --short HEAD)
else
    REPO_GIT_COMMIT=${REPO_GIT_COMMIT:0:12}
    echo "Build request suggests checking out a different commit $REPO_GIT_COMMIT" >&2
    (cd "$DIR/../sidekiq_sw"; \
     git submodule deinit -f --all; \
     git checkout $REPO_GIT_COMMIT; \
     git submodule sync --recursive)
fi

#
# Now that sidekiq_sw submodule is checked out to the desired commit
# (either what was committed or specified), bring in the rest of
# sidekiq_sw's submodules
#
(cd "$DIR/../sidekiq_sw"; \
     git submodule update --init --recursive)

if [ x"$REPO_GIT_REVISION" = x"committed" ]; then
    REPO_GIT_REVISION=$(cd "$DIR/../sidekiq_sw"; git name-rev --name-only "$REPO_GIT_COMMIT")
fi

FN=sidekiq_sw/host_code/libsidekiq/sidekiq_core/inc/sidekiq_api.h
MJ=$(grep -m1 LIBSIDEKIQ_VERSION_MAJOR ${FN} | awk '{print $3}')
MN=$(grep -m1 LIBSIDEKIQ_VERSION_MINOR ${FN} | awk '{print $3}')
PT=$(grep -m1 LIBSIDEKIQ_VERSION_PATCH ${FN} | awk '{print $3}')
LB=$(grep -m2 LIBSIDEKIQ_VERSION_LABEL ${FN} | tail -n1 | awk '{print $3}')
DT=$(date +%Y%m%d)
GH=g$REPO_GIT_COMMIT

echo "GIT_REVISION is $REPO_GIT_REVISION" >&2
echo "GIT_COMMIT is $REPO_GIT_COMMIT" >&2

if [[ "$REPO_GIT_REVISION" =~ "tags/" ]]; then
    VER=$MJ.$MN.$PT
elif [[ "$REPO_GIT_REVISION" =~ "/release/" ]]; then
    # if the LABEL is empty (after discarding double quote) do not use a qualifier in $VER
    if [ -z "${LB//\"}" ]; then
        VER=$MJ.$MN.$PT
    else
        VER=$MJ.$MN.$PT-${BUILD_NUMBER:-manual}-rc
    fi
else
    VER=$MJ.$MN.$PT-${BUILD_NUMBER:-manual}-$GH
fi

echo "VER is $VER" >&2

echo "==================== BUILDING SDK ====================" >&2
(cd sidekiq_sw; make BUILD_CONFIG=mingw64 -j4)

echo "==================== PREPARING INSTALLER ====================" >&2
(cd installer/windows; ./prepare_installer.sh)

echo "==================== BUILDING INSTALLER ====================" >&2
(cd installer/windows; ./make_installer.sh)

echo "==================== RENAMING INSTALLER ====================" >&2
(cd installer/windows; mv SidekiqSdk_*_setup.exe SidekiqSdk_${VER}_setup.exe)

# unregister the trap
trap - 0
