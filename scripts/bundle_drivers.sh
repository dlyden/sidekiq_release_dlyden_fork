#! /bin/bash

if [ $# -lt 1 ]; then
    echo "usage: $(basename $(readlink -f $0)) <version> [ <version> ... ]" >&2
    exit 1
fi

D=$(dirname $(readlink -f $0))

PCI=$(readlink -f $D/../libraries/pci_manager/driver)
DMA=$(readlink -f $D/../libraries/nw_logic_dma/dma_driver/DMADriver/Linux/src)

TAG=$(cd $D/../sidekiq_sw; git describe --tags)

for VER; do
    ALL_FOUND=yes
    MKT=$(mktemp -d)
    for d in driver gps uart; do
        if [ -d drivers/sidekiq_${d}_release/output/*/$VER ]; then
            # create a temporary directory, copy modules there, tar it up
            mkdir -p $MKT/$VER
            cp $D/../drivers/sidekiq_${d}_release/output/*/$VER/*.ko $MKT/$VER
        else
            echo "$VER for sidekiq_${d}_release is missing!"
            ALL_FOUND=no
        fi
    done

    if [ x"$ALL_FOUND" = x"yes" ]; then
        FN=linux_kernel-$VER-for-$TAG.tar.xz
        XZ_OPT=-9ev tar --owner=root --group=root --mtime 'UTC 1970-01-01' -cJf $FN -C $MKT $VER

        tar tvf $FN
        echo "  bundled drivers in '$FN'" >&2
    fi
    rm -rf $MKT
done
