#!/bin/bash
# Prepares files and directories to be used by the installer, make_installer.sh
# This entails moving/copying some files to other/additional locations and
# cleaning up some build artifacts (e.g., object files, etc.) so that entire
# directories can be added to the installer (rather than specifying each file).

REPO_ROOT=../../sidekiq_sw

# Copy DLLs required by the test apps to the test app directory
cp -v $REPO_ROOT/build.mingw64/lib/libsidekiq__mingw64.dll \
    $REPO_ROOT/libraries/nw_logic_dma/dma_driver/DMADriver_2019_02_15/DMADriver-2015/DMADriver-2015/TestCli/DmaDriverDLL/x64/Release/DmaDriverDllx64.dll \
    $REPO_ROOT/host_code/libsidekiq/test_apps/bin/mingw64/

# Clean the Visual Studio builds
for d in versionTest RxBenchmark; do
    (cd $REPO_ROOT/host_code/libsidekiq/test_apps.win64/visual_studio/$d;
     /c/Program\ Files\ \(x86\)/MSBuild/14.0/Bin/MSBuild.exe -p:Configuration="Release" -p:Platform="x64" -t:Clean)
done

# Dereference FPGA bitstream symlinks here since NSIS can't
rm -rf fpga_images
mkdir -p fpga_images
for fn in $REPO_ROOT/sidekiq_fpga_bitstreams/sidekiq_image_*.bi?; do
    if [ $(stat --printf "%s" "$fn") -le 1024 ]; then
	# probably a symlink, so resolve by looking inside
	cp -v $(dirname "$fn")/$(cat "$fn") fpga_images/$(basename "$fn")
    else
	cp -v "$fn" fpga_images/
    fi
done

# unbutton the documentation if present, otherwise ask the user if it's okay to proceed without
#
# grab the sdk.zip from a previous sidekiq-release build
#   -- https://jenkins.epiq.rocks/job/Sidekiq/job/sidekiq-release/lastSuccessfulBuild/artifact/components/sdk/*zip*/sdk.zip
#
if [ -e sdk.zip ]; then
    rm -rf sdk/
    unzip sdk.zip
    SDK=$(find sdk/ -regex '.*/sidekiq_sdk_[^_]*\.tar\.xz' | head -n1)
    tar -xvf "$SDK" --wildcards --strip-components=1 '*/doc/api' '*/doc/sdk_manual' '*/doc/manuals'
else
    echo "no sdk.zip found!" >&2
    exit 1
fi
