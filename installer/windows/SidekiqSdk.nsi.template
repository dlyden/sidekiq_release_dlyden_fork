;--------------------------------
; SidekiqSdk.nsi
;
; This script is based on example2.nsi.  It remembers the directory, 
; has uninstall support and (optionally) installs start menu shortcuts.
;
; It will install the SidekiqSdk into a directory that the user selects.


;--------------------------------
;--------------------------------
; INSTALLER CONFIGURATION
;--------------------------------
;--------------------------------
; To use the "new" UI
!include MUI2.nsh

; To test for 32-bit vs. 64-bit
!include x64.nsh

!include "Sections.nsh"
# SECTION_OFF is defined in Sections.nsh as 0xFFFFFFFE
!define SECTION_ON ${SF_SELECTED} # 0x1

; Use a better but slower compressor
SetCompressor /FINAL /SOLID lzma
SetCompressorDictSize 64
SetDatablockOptimize On

;--------------------------------
; Definitions
!define COMPANY "Epiq Solutions"
!define PRODUCT "Sidekiq SDK"

!ifndef VERSION
!define VERSION "%MAJOR%.%MINOR%.%PATCH%.0"
!endif

!define COMPANY_WEB_SITE "https://www.epiqsolutions.com/"
!define COMPANY_ICON_256x256 "epiq_logo_256x256.ico"
!define COMPANY_ICON_256x168 "epiq_logo_256x168.ico"
!define COMPANY_BMP_750x493  "epiq_logo_750x493.bmp"
!define COMPANY_BMP_113x74   "epiq_logo_113x74.bmp"


;--------------------------------
; Variables

;--------------------------------
; The name of the installer
Name "Sidekiq SDK ${VERSION}"

; The file to write
OutFile "SidekiqSdk_${VERSION}_setup.exe"

; The default installation directory
InstallDir "$PROGRAMFILES\${COMPANY}\${PRODUCT}\${VERSION}"

; Request application privileges for Windows Vista
RequestExecutionLevel admin

; Interface Configuration
; No AutoClose (allow user to review install log before Finish page)
!define MUI_FINISHPAGE_NOAUTOCLOSE

;--------------------------------
; Icon
!define MUI_ICON ${COMPANY_ICON_256x168}
!define MUI_UNICON ${COMPANY_ICON_256x168}

;--------------------------------
; Image
!define MUI_HEADERIMAGE
!define MUI_HEADERIMAGE_BITMAP ${COMPANY_BMP_113x74}
!define MUI_HEADERIMAGE_RIGHT

!define MUI_UNHEADERIMAGE
!define MUI_UNHEADERIMAGE_BITMAP ${COMPANY_BMP_113x74}
!define MUI_UNHEADERIMAGE_RIGHT


;--------------------------------
;--------------------------------
; INSTALLER PAGES
;--------------------------------
;--------------------------------
; Pages

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH


!insertmacro MUI_UNPAGE_WELCOME
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_UNPAGE_FINISH

  
;--------------------------------
;Languages
 
!insertmacro MUI_LANGUAGE "English"


;--------------------------------
; Description of the Installer executable
VIProductVersion "${VERSION}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductName" "${PRODUCT}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "CompanyName" "${COMPANY}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "FileDescription" "Installer for ${PRODUCT}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "FileVersion" "${VERSION}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductVersion" "${VERSION}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalCopyright" "© 2021 Epiq Solutions"


;--------------------------------
; Macro to create an internet shortcut
!macro CreateInternetShortcut FILENAME URL ICONFILE ICONINDEX
WriteINIStr "${FILENAME}.url" "InternetShortcut" "URL" "${URL}"
WriteINIStr "${FILENAME}.url" "InternetShortcut" "IconFile" "${ICONFILE}"
WriteINIStr "${FILENAME}.url" "InternetShortcut" "IconIndex" "${ICONINDEX}"
!macroend


;--------------------------------
;--------------------------------
; INSTALLER FILES TO COPY
;--------------------------------
;--------------------------------


;--------------------------------
; Sidekiq SDK Required Files
;--------------------------------
Section "!${PRODUCT} (required)"  Sidekiq_SDK_Required_Section

  SectionIn RO
  
  ; Add a link to the company Web site
  SetOutPath $INSTDIR
  File "${COMPANY} Website.URL"
  File ${COMPANY_ICON_256x168}
  
  ; Put SDK files there (headers and libraries)
  SetOutPath $INSTDIR\sidekiq_sdk
  File /a /r "..\..\sidekiq_sw\build.mingw64\"

  ; Include documentation
  SetOutPath $INSTDIR\sidekiq_sdk\doc
  File /a /r "doc\*"

  SetOutPath $INSTDIR\sidekiq_sdk\bin
  File /a "c:\msys64\mingw64\bin\libglib-2.0-0.dll"
  File /a "c:\msys64\mingw64\bin\libwinpthread-1.dll"
  File /a "c:\msys64\mingw64\bin\libintl-8.dll"
  File /a "c:\msys64\mingw64\bin\libiconv-2.dll"
  File /a "c:\msys64\mingw64\bin\libpcre-1.dll"
  File /a "c:\msys64\mingw64\bin\libgcc_s_seh-1.dll"
  File /a "..\..\sidekiq_sw\libraries\libusb\build.mingw64\support\usr\lib\bin\libusb-1.0.dll"

  ; Write the installation path into the registry
  WriteRegStr HKLM "SOFTWARE\${COMPANY}\${PRODUCT}" "Install_Dir" "$INSTDIR"

  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT}" "DisplayName" "${PRODUCT}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT}" "UninstallString" "$INSTDIR\uninstall.exe"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT}" "DisplayVersion" "${VERSION}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT}" "DisplayIcon" "$INSTDIR\uninstall.exe"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT}" "Publisher" "${COMPANY}"
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT}" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT}" "NoRepair" 1
  WriteUninstaller "uninstall.exe"
  
SectionEnd


;--------------------------------
; Sidekiq FPGA Image Files
;--------------------------------
Section "-Sidekiq_FpgaImages (hidden section)"  Sidekiq_FpgaImages_Section

  ; Set output path to the installation directory.
  SetOutPath $INSTDIR\fpga_images
  
  ; Include the FPGA bitstream files, which would be required by prog_fpga.exe
  File /a /r "fpga_images\sidekiq_image_*.bi?"
  File /a "..\..\sidekiq_sw\sidekiq_fpga_bitstreams\README"

SectionEnd

  


;--------------------------------
; Sidekiq SDK DMA Device Driver Files
;--------------------------------
Section "-Sidekiq_DmaDriver (hidden section)"  Sidekiq_DmaDriver_Section

  ; Set output path to the installation directory.
  SetOutPath $INSTDIR\driver\x64
  
  ; Include the 64-bit DMA driver package
  ; and the DMA driver interface library
  ; This is the latest signed driver:
  FILE /a /r ..\..\sidekiq_sw\libraries\nw_logic_dma\dma_driver\DMADriver_2019_02_15\releases\2021_05_12_v5.4.5

  ; Include USB driver Setup Information file and Security Catalog for the USB interface
  FILE /a /r ..\..\sidekiq_sw\utils\usb_driver\sidekiq_usb.inf
  FILE /a /r ..\..\sidekiq_sw\utils\usb_driver\sidekiq_usb.cat

SectionEnd

;--------------------------------
; Sidekiq SDK Driver interface Files
;--------------------------------
Section "-Sidekiq_DmaDriverInterface (hidden section)"  Sidekiq_DmaDriverInterface_Section

  ; Set output path to the installation directory.
  SetOutPath $INSTDIR\driver_lib
  
  ; Include the DMA driver package
  ; and the DMA driver interface library
  FILE ..\..\sidekiq_sw\libraries\nw_logic_dma\dma_driver\DMADriver_2019_02_15\DMADriver-2015\DMADriver-2015\TestCli\DmaDriverDLL\x64\Release\DmaDriverDllx64.lib
  FILE ..\..\sidekiq_sw\libraries\nw_logic_dma\dma_driver\DMADriver_2019_02_15\DMADriver-2015\DMADriver-2015\TestCli\DmaDriverDLL\x64\Release\DmaDriverDllx64.dll

SectionEnd

;--------------------------------
; Sidekiq SDK Test Applications
;--------------------------------
; Optional section (can be disabled by the user)
Section "${PRODUCT} Test Applications"  Sidekiq_SDK_TestApp_Section

  ; Set output path to the installation directory.
  SetOutPath $INSTDIR\sidekiq_sdk\bin
  
  ; Put files there (full directory)
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\bin\mingw64\app_src_file1.exe"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\bin\mingw64\fdd_rx_tx_samples.exe"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\bin\mingw64\multicard_rx_samples.exe"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\bin\mingw64\multicard_tx_samples.exe"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\bin\mingw64\pps_tester.exe"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\bin\mingw64\prog_fpga.exe"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\bin\mingw64\read_accel.exe"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\bin\mingw64\read_gpsdo.exe"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\bin\mingw64\read_temp.exe"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\bin\mingw64\ref_clock.exe"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\bin\mingw64\rx_benchmark.exe"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\bin\mingw64\rx_samples.exe"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\bin\mingw64\rx_samples_freq_hopping.exe"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\bin\mingw64\rx_samples_minimal.exe"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\bin\mingw64\rx_samples_on_trigger.exe"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\bin\mingw64\set_rx_LO_freq.exe"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\bin\mingw64\sidekiq_probe.exe"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\bin\mingw64\store_user_fpga.exe"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\bin\mingw64\sweep_receive.exe"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\bin\mingw64\tdd_rx_tx_samples.exe"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\bin\mingw64\test_golden_present.exe"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\bin\mingw64\tx_benchmark.exe"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\bin\mingw64\tx_configure.exe"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\bin\mingw64\tx_samples_on_1pps.exe"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\bin\mingw64\tx_samples.exe"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\bin\mingw64\user_reg_test.exe"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\bin\mingw64\version_test.exe"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\bin\mingw64\xcv_benchmark.exe"

  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\bin\mingw64\libsidekiq__mingw64.dll"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\bin\mingw64\DmaDriverDllx64.dll"

SectionEnd

;--------------------------------
; Sidekiq SDK Test Application Sources
;--------------------------------
; Optional section (can be disabled by the user)
Section "${PRODUCT} Test Application (Source Files)"  Sidekiq_SDK_TestAppSources_Section

  ; Set output path to the installation directory.
  SetOutPath $INSTDIR\sidekiq_sdk

  File /a /oname=Makefile "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\Makefile.for_win_sdk"
  File /a /oname=tools.mk "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\tools.mk.for_win_sdk"

  ; Install arg_parser library and header
  SetOutPath $INSTDIR\sidekiq_sdk\arg_parser\inc
  File /a "..\..\sidekiq_sw\host_code\arg_parser\inc\arg_parser.h"

  SetOutPath $INSTDIR\sidekiq_sdk\arg_parser\lib
  File /a "..\..\sidekiq_sw\host_code\arg_parser\arg_parser__mingw64.a"

  ; Set output path to the installation directory.
  SetOutPath $INSTDIR\sidekiq_sdk\src
  
  ; Put SDK source files there (full directory)
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\src\app_src_file1.c"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\src\elapsed.h"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\src\fdd_rx_tx_samples.c"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\src\multicard_rx_samples.c"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\src\multicard_tx_samples.c"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\src\pps_tester.c"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\src\prog_fpga.c"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\src\read_accel.c"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\src\read_gpsdo.c"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\src\read_temp.c"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\src\rx_benchmark.c"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\src\rx_samples.c"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\src\rx_samples_freq_hopping.c"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\src\rx_samples_minimal.c"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\src\rx_samples_on_trigger.c"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\src\set_rx_LO_freq.c"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\src\sidekiq_probe.c"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\src\store_user_fpga.c"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\src\sweep_receive.c"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\src\tdd_rx_tx_samples.c"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\src\test_golden_present.c"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\src\tx_benchmark.c"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\src\tx_configure.c"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\src\tx_samples.c"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\src\tx_samples_on_1pps.c"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\src\user_reg_test.c"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\src\version_test.c"
  File /a "..\..\sidekiq_sw\host_code\libsidekiq\test_apps\src\xcv_benchmark.c"

  ; Set output path to the installation directory.
  SetOutPath $INSTDIR\sidekiq_sdk\src\examples
  
  File /a /r /x .gitignore "..\..\sidekiq_sw\host_code\libsidekiq\test_apps.win64\visual_studio\"

SectionEnd

;--------------------------------
; Start Menu Shortcuts
;--------------------------------
; Optional section (can be disabled by the user)
Section "Start Menu Shortcuts"  StartMenuShortcut_Section

  CreateDirectory "$SMPROGRAMS\${COMPANY}\${PRODUCT}"
  
  !insertmacro CreateInternetShortcut \
  "$SMPROGRAMS\${COMPANY}\${COMPANY} homepage" \
  ${COMPANY_WEB_SITE} \
  "$InstDir\${COMPANY_ICON_256x168}" "0"
  
  CreateShortcut "$SMPROGRAMS\${COMPANY}\${PRODUCT}\Uninstall.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe" 0
  
SectionEnd

;--------------------------------
;--------------------------------
; UNINSTALL
;--------------------------------
;--------------------------------
; Uninstaller

Section "Uninstall"
  
  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT}"
  DeleteRegKey HKLM "SOFTWARE\${COMPANY}\${PRODUCT}"

  ; Remove Sidekiq_SDK_Required_Section files
  ; Also removes Sidekiq_SDK_TestApp_Section and Sidekiq_SDK_TestAppSources_Section files
  RmDir /r "$INSTDIR\sidekiq_sdk\"
  
  ; Remove Sidekiq_FpgaImages_Section files
  RmDir /r "$INSTDIR\fpga_images\"
  
  ; Remove Sidekiq_DmaDriver_Section files
  RmDir /r "$INSTDIR\driver\"
  ; Remove Sidekiq_DmaDriverInterface_Section files
  RmDir /r "$INSTDIR\driver_lib\"
  

  ; Remove StartMenuShortcut_Section files
  Delete "$INSTDIR\${COMPANY} Website.URL"
  Delete "$INSTDIR\${COMPANY_ICON_256x168}"

  ; Remove uninstaller
  Delete $INSTDIR\uninstall.exe

  ; Remove shortcuts, if any
  Delete "$SMPROGRAMS\${COMPANY}\${PRODUCT}\*.*"

  ; Remove directories used
  RMDir "$SMPROGRAMS\${COMPANY}\${PRODUCT}"
;  RMDir "$SMPROGRAMS\${COMPANY}"
  RMDir "$INSTDIR"

SectionEnd

;--------------------------------
;--------------------------------
; Startup Code
;--------------------------------
;--------------------------------

; Nothing at the moment

;--------------------------------
;--------------------------------
; DESCRIPTIONS OF SECTIONS FOR INSTALLATION
;--------------------------------
;--------------------------------
LangString DESC_StartMenuShortcut_Section ${LANG_ENGLISH} "Shortcuts on the Start Menu."
LangString DESC_Sidekiq_SDK_Required_Section ${LANG_ENGLISH} "Core Sidekiq SDK components."
LangString DESC_Sidekiq_SDK_TestApp_Section ${LANG_ENGLISH} "Sidekiq SDK test applications."
LangString DESC_Sidekiq_SDK_TestAppSources_Section ${LANG_ENGLISH} "Sidekiq SDK test application source files."


!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
  !insertmacro MUI_DESCRIPTION_TEXT ${Sidekiq_SDK_Required_Section} $(DESC_Sidekiq_SDK_Required_Section)
  !insertmacro MUI_DESCRIPTION_TEXT ${Sidekiq_SDK_TestApp_Section} $(DESC_Sidekiq_SDK_TestApp_Section)
  !insertmacro MUI_DESCRIPTION_TEXT ${Sidekiq_SDK_TestAppSources_Section} $(DESC_Sidekiq_SDK_TestAppSources_Section)
  !insertmacro MUI_DESCRIPTION_TEXT ${StartMenuShortcut_Section} $(DESC_StartMenuShortcut_Section)
!insertmacro MUI_FUNCTION_DESCRIPTION_END
