#!/bin/bash

REPO_ROOT=../../sidekiq_sw

# grab major, minor, patch versions from sidekiq_api.h
MJ=$(grep -m1 LIBSIDEKIQ_VERSION_MAJOR $REPO_ROOT/build.mingw64/include/sidekiq_api.h | awk '{print $3}')
MN=$(grep -m1 LIBSIDEKIQ_VERSION_MINOR $REPO_ROOT/build.mingw64/include/sidekiq_api.h | awk '{print $3}')
PT=$(grep -m1 LIBSIDEKIQ_VERSION_PATCH $REPO_ROOT/build.mingw64/include/sidekiq_api.h | awk '{print $3}')

# make substitutions in the .template file
sed -e "s/%MAJOR%/$MJ/g;s/%MINOR%/$MN/g;s/%PATCH%/$PT/g" SidekiqSdk.nsi.template > SidekiqSdk.nsi

# compile the installer!
/c/Program\ Files\ \(x86\)/NSIS/makensis -P2 -V4 SidekiqSdk.nsi
