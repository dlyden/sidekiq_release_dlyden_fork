
To build the baseline image (just once!):

   docker build -f Dockerfile-baseline -t docker.epiq.rocks/extra-kernel-modules:initial .

To build the image that compiles kernel modules with the latest kernel headers for the NVIDIA Jetson
and Gateworks kernels:

   docker build -t docker.epiq.rocks/extra-kernel-modules:v4.17.4 .
