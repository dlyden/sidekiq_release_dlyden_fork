#!/bin/sh

URL=https://server03.epiq.rocks/mnt/storage/projects/sidekiq/sw/kernel-headers

MKT=$(mktemp -d)

echo "installonlypkgs=kernel-devel" > /etc/yum.conf

for SUBDIR; do
    curl -sS $URL/$SUBDIR | \
        awk 'match($0, /kernel-devel[^>]*.rpm/) {
    print substr($0, RSTART, RLENGTH)
}
' | \
        sed -e "s,^,$URL/$SUBDIR/," | \
        (cd $MKT; xargs -r -n1 curl -O)
done

(cd $MKT; ls; rpm --force -ivh kernel-devel*.rpm)
rm -rf $MKT

yum clean all
rm -rf /var/cache/yum
