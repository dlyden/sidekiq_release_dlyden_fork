#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Usage: $0 <libsidekiq-version vX.Y.Z>" >&2
    echo >&2
    echo "Example: $0 v4.17.5" >&2
    exit 1
fi

IMAGE=docker.epiq.rocks/$(basename $PWD)
VER="$1"

docker build -f Dockerfile -t $IMAGE:$VER .
