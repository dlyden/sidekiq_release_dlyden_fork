#! /bin/bash

# exit on any error
set -o errexit

# Make sure the image date and SDK version are specified
if [ $# -ne 2 ]; then
    echo "usage: $(basename $(readlink -f $0)) <image_version> <SDK_version>" >&2
    echo >&2
    echo "    image_version is in the form YYYYMMDD" >&2
    echo "    SDK_version is in the form X.Y.Z" >&2
    echo >&2
    exit 1
fi

D=$(dirname $(readlink -f $0))
source $D/shared-functions

img_ver=$1
sdk_ver=$2
image_dir=sidekiq_image_z3u_v${sdk_ver}_${img_ver}

# Create the image directory
rm -rf $D/$image_dir
mkdir -p $D/$image_dir/pkgs

# copy driver, utils, fpga
tar -C $D/common -cf - utils | tar -C $D/$image_dir -xf -
(cd $D/common && tar --exclude .git --exclude pdk -chf - fpga/README fpga/sidekiq_image_z3u_* | tar -C $D/$image_dir -xf -)

# symlink remaining FPGA bitstreams
symlink-latest-fpga $D/$image_dir/fpga

# Copy the README.txt from sidekiq_sw submodule
cp $D/../sidekiq_sw/host_code/libsidekiq/README.txt $D/$image_dir/README.txt

# Create the skiq_daemon package
(cd base_structure_z3u;
 ./package_z3u_skiq_daemon.sh ${sdk_ver})
cp base_structure_z3u/z3u-skiq-daemon_${sdk_ver}_arm64.deb $D/$image_dir/pkgs

# bundle it up
tar -C $D --owner=root --group=root -cvf $image_dir.tar $image_dir
xz -9vf $image_dir.tar
