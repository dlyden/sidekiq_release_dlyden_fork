#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Must specify version"
    exit 1
fi
VERS=$1

D=$(dirname "$(readlink -f "$0")")

PREBUILT_APPS=$D/../../sdk_release/sidekiq_sdk_v$1_prebuilt_apps
SERVICE_USR_SBIN=./service/usr/sbin/

if [[ ! -d $PREBUILT_APPS ]]
then
    echo "Cannot find Sidekiq SDK build for version $VERS at $PREBUILT_APPS"
    exit 1
fi

rm -rf z3u-skiq-daemon*.deb
rm -rf $SERVICE_USR_SBIN

mkdir -p $SERVICE_USR_SBIN
if [ -d $SERVICE_USR_SBIN ] 
then
    cp "$PREBUILT_APPS/z3u/skiq_daemon" "$SERVICE_USR_SBIN"
    fpm --after-install=z3u-skiq-daemon-post-upgrade.sh \
        -s dir \
        -t deb \
        --architecture=arm64 \
        --vendor 'Epiq Solutions <info@epiqsolutions.com>' \
        --maintainer 'Epiq Solutions <info@epiqsolutions.com>' \
        --url 'https://epiqsolutions.com' \
        --description 'Z3u Sidekiq Daemon' \
        --depends 'z3u-init >= 1.0.1' \
        --name z3u-skiq-daemon \
        --version "$VERS" \
        ./service/=/
else
    echo "Error: Directory /path/to/dir $SERVICE_USR_SBIN does not exist."
fi




