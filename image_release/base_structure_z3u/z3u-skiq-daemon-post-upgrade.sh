#!/bin/sh
if [ -f /etc/systemd/system/multi-user.target.wants/z3u_skiq_daemon.service ]; then
    systemctl stop z3u_skiq_daemon
    systemctl disable z3u_skiq_daemon
    systemctl daemon-reload
    systemctl reset-failed
fi

systemctl enable z3u_skiq_daemon.service
