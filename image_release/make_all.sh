#! /bin/bash

# exit on any error
set -o errexit

# Make sure the image date and SDK version are specified
if [ $# -ne 2 ]; then
    echo "usage: $(basename $(readlink -f $0)) <image_version> <SDK_version>" >&2
    echo >&2
    echo "    image_version is in the form YYYYMMDD" >&2
    echo "    SDK_version is in the form X.Y.Z" >&2
    echo >&2
    exit 1
fi

function perform()
{
    echo "--> $@" >&2
    "$@"
}

D=$(dirname $(readlink -f $0))
IMAGE=$1
SDK=$2

perform $D/make_image.sh $IMAGE $SDK
perform $D/make_image_z2.sh $IMAGE $SDK
perform $D/make_image_z2_armhf.sh $IMAGE $SDK
perform $D/make_image_openwrt_glibc.sh $IMAGE $SDK
perform $D/make_image_jetson.sh $IMAGE $SDK
perform $D/make_image_z3u.sh $IMAGE $SDK

echo >&2
echo "output left in:" >&2
ls -lh sidekiq_image_v${SDK}_${IMAGE}.tar.xz \
    sidekiq_${IMAGE}-${SDK}-1_arm.opk \
    sidekiq_${IMAGE}-${SDK}-1_armhf.opk \
    sidekiq_${IMAGE}-${SDK}-glibc-1_imx6.opk \
    sidekiq_image_jetson_v${SDK}_${IMAGE}.tar.xz \
    sidekiq_image_z3u_v${SDK}_${IMAGE}.tar.xz
echo >&2
