#! /bin/bash

# exit on any error
set -o errexit

# Make sure the image date and SDK version are specified
if [ $# -ne 2 ]; then
    echo "usage: $(basename $(readlink -f $0)) <image_version> <SDK_version>" >&2
    echo >&2
    echo "    image_version is in the form YYYYMMDD" >&2
    echo "    SDK_version is in the form X.Y.Z" >&2
    echo >&2
    exit 1
fi

D=$(dirname $(readlink -f $0))

PLATFORM=z2-armhf
image_dir=sidekiq_image_z2-armhf_v$2_$1

if [ ! -e $D/../sdk_release/sidekiq_sdk_v$2_prebuilt_apps/$PLATFORM/version_test ]; then
    echo "!!! test applications for PLATFORM $PLATFORM are missing, exiting !!!" >&2
    exit 1
fi

# Create the image directory
rm -rf $D/$image_dir
mkdir -p $D/$image_dir

# Copy the base components
tar -C $D/base_structure_z2_armhf -cf - . | tar -C $D/$image_dir -xvf -

# modify CONTROL/control according to script params
sed -i -e "s,DATE,$1,g" $D/$image_dir/CONTROL/control
sed -i -e "s,SDK,$2,g" $D/$image_dir/CONTROL/control

# mkdir and symlink
mkdir -p $D/$image_dir/mnt/app_rw/usr/bin/epiq

# copy over utils and re-write interpreter from /bin/bash to /bin/sh
tar -C $D/common/utils -cf - . | tar -C $D/$image_dir/mnt/app_rw/usr/bin/epiq -xvf -
sed -i -e 's,^#!/bin/bash$,#!/bin/sh,g' $D/$image_dir/mnt/app_rw/usr/bin/epiq/*.sh

# copy over the test apps
echo "copying test apps:"
(cd $D/../sdk_release/sidekiq_sdk_v${2}_prebuilt_apps/$PLATFORM;
 (find . -type l; echo skiq_multi_call) \
     | xargs tar -cf - \
     | tar -C $D/$image_dir/mnt/app_rw/usr/bin/epiq -xvf - \
     | sed -e 's,^./,,;s,^,\t,')

# remove group-write permissions throughout
chmod -R g-w $D/$image_dir

# create the opkg
fakeroot opkg-build -O $image_dir
