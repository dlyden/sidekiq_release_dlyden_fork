#!/bin/bash

VERSION_TOOL=/usr/bin/versions
VERSION_TEST_TOOL=~/sidekiq_image_current/test_apps/version_test
LOGREAD_TOOL=/sbin/logread
JOURNAL_TOOL=/bin/journalctl
OPKG_TOOL=/bin/opkg
DPKG_TOOL=/usr/bin/dpkg
LOG_NUM_ENTRIES=100
COMPRESS_LOG=1

CURDATE=$(date +"%Y-%m-%d-%H-%M-%S")
LOGFILE=/tmp/sysInfo-$CURDATE.log

runCmd() {
    local banner="$1"
    shift

    echo -e "$banner" | tee -a "$LOGFILE"
    "$@" >>"$LOGFILE" 2>&1
    echo >>"$LOGFILE"
}

runCmdLimit() {
    local banner="$1"
    local limit="$2"
    shift 2

    echo -e "$banner" | tee -a "$LOGFILE"
    "$@" 2>&1 | tail -n "$limit" >> "$LOGFILE"
    echo >>"$LOGFILE"
}

getVersionInfo() {
    local d vt found
    found=0

    # get a list of users' home directories and look for version_test
    for d in $(cut -d: -f6 /etc/passwd); do
        vt=$d/sidekiq_image_current/test_apps/version_test
        if [ -e "$vt" -a -x "$vt" ]; then
            runCmd "$1\n--- Basic --- $vt" $vt
            runCmd "--- Full --- $vt --full" $vt --full
            found=1
        fi
    done

    if [ $found -eq 0 ]; then
        runCmd "$1" echo "Cannot find version tool!"
    fi
}

getSystemLog() {
    if [  -e "$LOGREAD_TOOL" -a -x "$LOGREAD_TOOL" ]; then
        runCmd "$1" logread -l $LOG_NUM_ENTRIES
    elif [ -e /var/log/syslog ]; then
        runCmd "$1" tail -n $LOG_NUM_ENTRIES /var/log/syslog
    fi
}

getDmesg() {
    local options=""
    local text=""
    local res=0

    text=$(dmesg --help 2>&1 | grep -- "-P")
    res=$?
    if [ "$res" -eq 0 -a "$text" != "" ]; then
        options=" -P"
    fi

    runCmdLimit "$1" $LOG_NUM_ENTRIES dmesg $options
}

getJournal() {
    if [  -e "$JOURNAL_TOOL" -a -x "$JOURNAL_TOOL" ]; then
        runCmd "$1" $JOURNAL_TOOL -b
    else
        runCmd "$1" echo "No journalctl tool available"
    fi
}

getL4TRelease() {
    local NV=/etc/nv_tegra_release
    if [  -e "$NV" ]; then
        runCmd "$1" head -n1 $NV
    else
        runCmd "$1" echo "'$NV' not available"
    fi
}

local_findDockerInCgroup() {
	local cgroup_file="$1"

	if grep -E "docker|kubepod" >/dev/null 2>&1 < "$cgroup_file"; then
		return 0
	fi

	return 1
}

runningInDocker() {
	local banner="$1"
	local status="Probably not"

	if [ -e "/.dockerenv" ]; then
		# The presence of this file may indicate that Docker is running
		status="Maybe"
	fi

	if local_findDockerInCgroup "/proc/self/cgroup"; then
		status="Yes"
	else
        if local_findDockerInCgroup "/proc/1/cgroup"; then
            status="Yes"
        fi
	fi

	runCmd "$banner" echo "Running in Docker container: $status"
}

readModules() {
    local d readmod

    # get a list of users' home directories and look for read_modules
    for d in $(cut -d: -f6 /etc/passwd); do
        readmod=$d/sidekiq_image_current/test_apps/read_modules
        if [ -e "$readmod" -a -x "$readmod" ]; then
            runCmd "$1\n--- $readmod --dma --pci" $readmod --dma --pci
        fi
    done

    if [ -e "/sys/module/dmadriver/version" ]; then
        runCmd "$1\nDMA Driver Version" cat /sys/module/dmadriver/version
    else
        runCmd "$1" echo "DMA Driver does not have version sysfs entry!"
    fi
}

lookupPids() {
    local banner="$1"
    local pids="$2"
    shift 2

    echo "$banner" >>"$LOGFILE" 2>&1
    text=$(ps uw $pids)
    res=$?
    if [ "$res" -eq 0 ]; then
        if [ "$text" != "" ]; then
            echo "$text" >>"$LOGFILE" 2>&1
        else
            echo "PID $pids not found in task list." >>"$LOGFILE" 2>&1
        fi
    else
        echo "Failed to run command ($res)."
    fi
}

pidsUsingSidekiq() {
    local banner="$1"
    local pids res

    pids=$(ls -l /proc/*/fd/* 2>/dev/null | grep DMAD | sed -n "s,.*/\([0-9]\+\)/fd/.*,\1,p")
    res=$?

    echo -e "$banner" | tee -a "$LOGFILE"
    if [ "$res" -ne 0 ]; then
        echo "Error: failed to run command ($?)!" >>"$LOGFILE" 2>&1
    else
        if [ "$pids" == "" ]; then
            echo "No processes found!" >>"$LOGFILE" 2>&1
        else
            lookupPids "The following PIDs seem to be using the Sidekiq: '$pids'." "$pids"
        fi
    fi

    echo >>"$LOGFILE"
}

main() {
    runCmd "System information (as of $CURDATE):\n" uname -a
    runCmd "=== Installed PCI devices ===" lspci -vvv
    runCmd "=== Installed USB devices ===" lsusb -v
    getVersionInfo "=== Version information ==="
    runningInDocker "=== Docker status ==="
    runCmd "=== Loaded modules ===" lsmod
    readModules "=== Read Modules ==="
    runCmd "=== Running programs ===" ps uawx
    pidsUsingSidekiq "=== PIDs using Sidekiq ==="
    getSystemLog "=== System log (last $LOG_NUM_ENTRIES entries) ==="
    getDmesg "=== Dmesg (last $LOG_NUM_ENTRIES entries) ==="
    getJournal "=== systemd journal ==="
    getL4TRelease "=== Linux4Tegra release info ==="

    if [ -e "$OPKG_TOOL" -a -x "$OPKG_TOOL" ]; then
        runCmd "=== Installed packages ===" opkg list-installed
    elif [ -e "$DPKG_TOOL" -a -x "$DPKG_TOOL" ]; then
        runCmd "=== Installed packages ===" dpkg -l
    else
        runCmd "=== Installed packages ===" echo "[ no package tool detected ]"
    fi
    echo
}

# Make sure only root can run our script
if [[ $EUID -ne 0 ]]; then
    echo "Warning: This script must be run as root, re-running with 'sudo'" 1>&2
    sudo "$0"
    exit $?
fi

# execute all logging commands
main

# scrub MAC addresses
sed -E -i "s/([[:xdigit:]]{1,2}:){5}[[:xdigit:]]{1,2}/XX:XX:XX:XX:XX:XX/" $LOGFILE

if [ "$COMPRESS_LOG" -eq 1 ]; then
    xz -V >/dev/null 2>&1
    if [ $? -eq 0 ]; then
        xz -6zce < "$LOGFILE" > "$LOGFILE.xz"
        COMP_LOGFILE=$LOGFILE.xz
    else
        gzip -6c < "$LOGFILE" > "$LOGFILE.gz"
        if [ $? -eq 0 ]; then
            COMP_LOGFILE=$LOGFILE.gz
        else
            echo "Failed to create compressed version of $LOGFILE ($res)" >&2
        fi
    fi
fi

echo -e "\tOutput copied to '$LOGFILE'" >&2
if [ -e "$COMP_LOGFILE" -a -s "$COMP_LOGFILE" ]; then
    echo -e "\tCompressed output stored as '$COMP_LOGFILE'" >&2
fi
echo >&2
