#! /bin/bash
# 
# This script determines the driver version required and loads the 
# appropriate kernel module versions

# Set the driver directory
if [ $# -eq 0 ]; then
    # No directory specified, use the script's base directory as the default location for the
    # modules
    D="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
else
    D="$1"
fi

# At this point the script assumes an absolute path so convert the driver directory
D="$(readlink -f "$D")"

if [ -z "$D" ]; then
    logger -s "Must specify path of directories containing drivers"
    exit 1
fi

# determine the kernel
version=$(uname -r)

if [ -e /etc/nv_tegra_release ]; then
    # Loading kernel modules on an NVIDIA Jetson platform is different because the kernel version is
    # the same across different L4T versions.  So extract the release and the revision from
    # /etc/nv_tegra_release, then append it to $D, then let the normal search go forward
    REL=$(head -n1 /etc/nv_tegra_release | awk '{ print tolower($2) }')
    REV=$(head -n1 /etc/nv_tegra_release | awk '{ print $5 }' | tr -d ',' | sed -e 's,\.0$,,')
    D="$D/$REL.$REV"
fi

# Look through the kernel directories for a match
if [ -e "$D/linux_kernel-$version" ]; then
    kernel_dir="$D/linux_kernel-$version"
elif [ -e "$D/$version" ]; then
    kernel_dir="$D/$version"
fi

is_system_module_present()
{
    local DRV
    local verbose
    local res
    local modinfo

    DRV="$(basename "$1" ".ko")"
    verbose="$2"
    res=0

    modinfo=$(command -v modinfo)
    if [ -z "$modinfo" ]; then
        logger -s "Error: failed to locate 'modinfo' utility"
        return 1
    fi

    $modinfo "$DRV" >/dev/null 2>&1 && res=0 || res=1

    if [ "$res" -eq "1" ] && [ x"$verbose" = x"yes" ]; then
        logger -s "Cannot find module '$DRV' in system module directories!"
    fi

    return $res
}

load_module()
{
    local mod_name="$1"
    local base_mod_name="$2"
    local res=0

    mkdir -p "$D/lib/modules/$version"
    res=$?

    if [ $res -eq 0 ] ; then
        touch "$D/lib/modules/$version/modules.builtin" "$D/lib/modules/$version/modules.order"
        res=$?
    fi
    if [ $res -eq 0 ] ; then
        ln --force --symbolic "$kernel_dir/$mod_name" "$D/lib/modules/$version/$mod_name"
        res=$?
    fi
    if [ $res -eq 0 ] ; then
        depmod --basedir "$D"
        res=$?
    fi
    if [ $res -eq 0 ] ; then
        modprobe --config "$D"/driver_config --dirname "$D" "$base_mod_name"
        res=$?
    fi

    return $res
}

load_optional_driver()
{
    local DRV="$1"
    local mod_name
    local base_mod_name
    local res=0

    mod_name=$(basename "$DRV")
    base_mod_name=$(basename "$mod_name" ".ko")

    if [ -e "$DRV" ] ; then
        load_module "$mod_name" "$base_mod_name"
        res=$?
    elif is_system_module_present "$base_mod_name" "no"; then
        modprobe "$base_mod_name"
        res=$?
    else
        logger -s "'$mod_name' not found for kernel version, skipping, some functionality may not be present"
    fi

    if [ $res -ne 0 ] ; then
        echo "Error: failed to load module $mod_name, some functionality may not be present"
        return $res
    fi
}

load_driver()
{
    local DRV="$1"
    local mod_name
    local base_mod_name
    local res=0

    mod_name=$(basename "$DRV")
    base_mod_name=$(basename "$mod_name" ".ko")

    if [ -e "$DRV" ] ; then
        load_module "$mod_name" "$base_mod_name"
        res=$?
    elif is_system_module_present "$base_mod_name" "no"; then
        modprobe "$base_mod_name"
        res=$?
    else
        logger -s "'$mod_name' not found for kernel version, skipping, other drivers may fail to load"
    fi

    if [ $res -ne 0 ] ; then
        echo "Error: failed to load module $mod_name, other drivers may fail to load"
        return $res
    fi
}

# Determine if we found the correct modules and load them
found=yes
if [ -z "$kernel_dir" ]; then
    logger -s "Pre-built modules not found for kernel version $version; trying system modules..."

    # Pre-build drivers were not found in the expected location; attempt to locate the required
    # modules residing in the standard kernel module paths (which should be the case if the
    # modules were built using DKMS)
    [ x"$found" = x"yes" ] && is_system_module_present "pci_manager" "yes" || found="no"
    [ x"$found" = x"yes" ] && is_system_module_present "dmadriver" "yes" || found="no"
    [ x"$found" = x"yes" ] && is_system_module_present "skiq_platform_device" "yes" || found="no"
fi

if [ x"$found" = x"no" ]; then
    logger -s "Kernel modules with version $version not found! Contact Epiq Solutions for driver support"
else
    logger -s "Sidekiq: Unloading any Sidekiq drivers"

    # unload drivers
    rmmod sidekiq_gps 2>/dev/null
    rmmod sidekiq_uart 2>/dev/null
    rmmod pci_manager 2>/dev/null
    rmmod dmadriver 2>/dev/null
    rmmod skiq_platform_device 2>/dev/null

    # load drivers - the underlying functions decide if the module comes from a specific directory
    # or from the system (which should be the case if the modules were built using DKMS)
    logger -s "Sidekiq: Loading Sidekiq drivers"
    load_driver "$kernel_dir/skiq_platform_device.ko"
    load_driver "$kernel_dir/dmadriver.ko"
    load_driver "$kernel_dir/pci_manager.ko"
    load_optional_driver "$kernel_dir/sidekiq_uart.ko"
    load_optional_driver "$kernel_dir/sidekiq_gps.ko"
fi
