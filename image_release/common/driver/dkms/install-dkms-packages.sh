#!/bin/bash
#
# This script installs Sidekiq DKMS packages for Debian-based or RedHat-based Linux distributions.
#
#

###########################################################################
# first things first, check for privileged user, the installer will
# need elevated privileges to install distro packages
#
if [ "$EUID" -ne 0 ]; then
    echo >&2
    echo -e "\tThis script must be run as root (or using sudo)" >&2
    echo >&2
    exit 1
fi

rp=$(realpath -e "$0")
D=$(dirname "$rp")

###########################################################################
# function: check-distro
function check-distro()
{
    local DISTRO="Unknown"
    if [ -x /usr/bin/lsb_release ]; then
        # grab the Distributor ID portion of the `lsb_release` output
        DISTRO=$(/usr/bin/lsb_release -a 2>/dev/null | grep 'Distributor ID:' | awk '{print $3}')

        # RHEL identifies as RedHatEnterpriseServer in lsb_release, so remove the EnterpriseServer
        # portion to make things a little cleaner
        DISTRO=${DISTRO%EnterpriseServer}
    elif [ -e /etc/redhat-release ]; then
        grep -iq 'centos' /etc/redhat-release
        [ $? -eq 0 ] && DISTRO="CentOS"
        grep -iq 'redhat' /etc/redhat-release
        [ $? -eq 0 ] && DISTRO="RedHat"
        grep -iq 'red hat' /etc/redhat-release
        [ $? -eq 0 ] && DISTRO="RedHat"
        grep -iq 'fedora' /etc/redhat-release
        [ $? -eq 0 ] && DISTRO="Fedora"
    fi

    echo "$DISTRO"
}

DISTRO=$(check-distro)
DISTRO_LC=$(echo $DISTRO | awk '{print tolower($0)}')
(cd $D; \
 case "$DISTRO_LC" in
     ubuntu|debian)
         dpkg -s dkms >/dev/null 2>&1
         if [ $? -eq 0 ]; then
             echo "Info: Installing DKMS packages (deb) for Sidekiq kernel modules" >&2
             dpkg -i deb/*.deb
             dkms status
         else
             echo "Error: Dynamic Kernel Module Support Framework (DKMS) is not installed" >&2
             echo "       Please install the 'dkms' package.  You can install it by typing:" >&2
             echo "       sudo apt install dkms" >&2
         fi
         ;;
     centos|fedora|redhat)
         rpm -ql dkms >/dev/null 2>&1
         if [ $? -eq 0 ]; then
             echo "Info: Installing DKMS packages (rpm) for Sidekiq kernel modules" >&2
             rpm -ivh rpm/*.rpm
             dkms status
         else
             echo "Error: Dynamic Kernel Module Support Framework (DKMS) is not installed" >&2
             echo "       Please install the 'dkms' package.  You can install it by typing:" >&2
             [[ "$DISTRO_LC" == "fedora" ]] || echo "       sudo yum install --enablerepo=extras epel-release" >&2
             echo "       sudo yum install dkms" >&2
         fi
         ;;
     *)
         echo "Error: No supported Linux distribution found; skipping installation." >&2
         ;;
 esac)
