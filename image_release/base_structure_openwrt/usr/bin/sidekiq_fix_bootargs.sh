#! /bin/sh

PATT="coherent_pool=4M"

echo "checking that bootargs contain $PATT" >&2
fw_printenv extra 2>/dev/null | grep -q $PATT
if [ $? -ne 0 ]; then
    echo "'extra' env needs update" >&2

    fw_printenv extra 2>/dev/null
    if [ $? -eq 0 ]; then
        # append
        NEW=$(fw_printenv extra | sed -e 's,^,coherent_pool=4M ,' | cut -d= -f 2-)
    else
        NEW="coherent_pool=4M"
    fi
    fw_setenv extra "$NEW"

    echo >&2
    echo "   REBOOT IS NECESSARY -- bootargs have changed" >&2
    echo >&2

    exit 1
else
    echo "no need for bootargs update" >&2

    exit 0
fi
