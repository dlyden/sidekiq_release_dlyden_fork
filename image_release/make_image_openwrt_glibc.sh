#! /bin/bash

# exit on any error
set -o errexit

# Make sure the image date and SDK version are specified
if [ $# -ne 2 ]; then
    echo "usage: $(basename $(readlink -f $0)) <image_version> <SDK_version>" >&2
    echo >&2
    echo "    image_version is in the form YYYYMMDD" >&2
    echo "    SDK_version is in the form X.Y.Z" >&2
    echo >&2
    exit 1
fi

D=$(dirname $(readlink -f $0))
source $D/shared-functions

PLATFORM=msiq-sx-1602
PLATFORM_RELEASE=${PLATFORM%"-1602"}
image_dir=sidekiq_image_openwrt_v$2_$1
SIC=$D/$image_dir/root/sidekiq_image_current

if [ ! -e $D/../sdk_release/sidekiq_sdk_v$2_prebuilt_apps/$PLATFORM_RELEASE/version_test ]; then
    echo "!!! test applications for PLATFORM $PLATFORM are missing, exiting !!!" >&2
    exit 1
fi

# Create the image directory
rm -rf $D/$image_dir
mkdir -p $D/$image_dir

# Copy the base components
tar -C $D/base_structure_openwrt -cf - . | tar -C $D/$image_dir -xvf -

# modify CONTROL/control according to script params
sed -i -e "s,DATE,$1,g" $D/$image_dir/CONTROL/control
sed -i -e "s,SDK,$2,g" $D/$image_dir/CONTROL/control
sed -i -e "s,-OPENWRT,-glibc,g" $D/$image_dir/CONTROL/control
sed -i -e "s,LIBS_DEPEND,sidekiq-shared-libs-arm-cortex-a9.gcc5.2-glibc-openwrt,g" $D/$image_dir/CONTROL/control

# mkdir and symlink
mkdir -p $D/$image_dir/root/sidekiq_image_v$2_$1
ln -snf sidekiq_image_v$2_$1 $SIC

# copy driver directory
tar -C $D/common --exclude dkms -cf - driver | tar -C $SIC -xf -

# copy FPGA bitstreams
copy-fpga-bitstreams $D $SIC

# remove unsupported FPGA images
(cd $SIC/fpga;
 rm -f sidekiq_x2*.bi? sidekiq_image_x2*.bi?;
 rm -f sidekiq_x4* sidekiq_image_x4*;
 rm -f sidekiq_image_m2_2280_*.bi?;
 rm -f sidekiq_image_nv100_*.bi?;
 rm -f sidekiq_image_z2*.bi?;
 rm -f sidekiq_image_z3u_*.bi?)

# symlink remaining FPGA bitstreams
symlink-latest-fpga $SIC/fpga

# copy all of the released drivers for the GW5xxx (32-bit ARM) distributions
copy-all-released-drivers $SIC/driver gw5xxx-openwrt
mv $SIC/driver/gw5xxx-openwrt/* $SIC/driver/
rmdir $SIC/driver/gw5xxx-openwrt/

# mv -f the load_sidekiq_drivers_openwrt.sh file which will overwrite the
# load_sidekiq_drivers.sh file without prompting
mv -f $SIC/driver/load_sidekiq_drivers_openwrt.sh $SIC/driver/load_sidekiq_drivers.sh

# copy over the test apps
echo "copying test apps:"
mkdir -p $SIC/test_apps
tar -C $D/../sdk_release/sidekiq_sdk_v$2_prebuilt_apps/$PLATFORM_RELEASE/ -cf - \
    fdd_rx_tx_samples \
    multicard_rx_samples \
    multicard_tx_samples \
    pps_tester \
    prog_fpga \
    read_accel \
    read_temp \
    ref_clock \
    rx_benchmark \
    rx_samples \
    rx_samples_freq_hopping \
    rx_samples_minimal \
    rx_samples_on_trigger \
    set_rx_LO_freq \
    sidekiq_probe \
    sweep_receive \
    store_user_fpga \
    tdd_rx_tx_samples \
    tx_benchmark \
    tx_configure \
    tx_samples \
    tx_samples_async \
    tx_samples_on_1pps \
    user_reg_test \
    version_test \
    xcv_benchmark \
    read_modules \
    | tar -C $SIC/test_apps -xvf - \
    | sed -e 's,^,\t,'

# copy utility scripts as well
tar -C $D/common/utils -cf - \
    system-report.sh \
    | tar -C $SIC/test_apps -xvf - \
    | sed -e 's,^,\t,'

# make sure /root permissions are acceptable
chmod 700 $image_dir/root

# create the opkg
fakeroot opkg-build -c -O $image_dir
