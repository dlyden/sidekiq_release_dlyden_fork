#! /bin/bash

# exit on any error
set -o errexit

# Make sure the image date and SDK version are specified
if [ $# -ne 2 ]; then
    echo "usage: $(basename "$(readlink -f "$0")") <image_version> <SDK_version>" >&2
    echo >&2
    echo "    image_version is in the form YYYYMMDD" >&2
    echo "    SDK_version is in the form X.Y.Z" >&2
    echo >&2
    exit 1
fi

D=$(dirname "$(readlink -f "$0")")
source "$D"/shared-functions

image_dir=sidekiq_image_v$2_$1

# Create the image directory
rm -rf "${D:?}"/"${image_dir:?}"
mkdir -p "$D"/"$image_dir"

# copy driver, utils
tar -C "$D"/common -cf - driver utils | tar -C "$D"/"$image_dir" -xf -

# copy FPGA bitstreams
copy-fpga-bitstreams "$D" "$D"/"$image_dir"

# remove unsupported FPGA images
(cd "$D"/"$image_dir"/fpga;
 rm -f sidekiq_image_z2p_*.bi?)

# symlink remaining FPGA bitstreams
symlink-latest-fpga "$D"/"$image_dir"/fpga

# copy all of the released drivers for the x86 (64-bit) distributions
copy-all-released-drivers "$D"/"$image_dir"/driver x86-centos x86-debian x86-fedora x86-ubuntu

# copy DKMS packages
copy-dkms-packages "$D"/"$image_dir"/driver

# bundle the drivers for each distribution
for d in "$(readlink -f "$D"/"$image_dir"/driver)"/*; do
    if [ -d "$d" ] && [ x"$d" = x"${d%dkms}" ] && [ x"$d" = x"${d%driver_config}" ]; then
        tar -C "$d" --owner=root --group=root -cf "$d".tar .
        xz -9vf "$d".tar
        rm -rf "$d"
    fi
done

# Copy the README.txt from sidekiq_sw submodule
cp "$D"/../sidekiq_sw/host_code/libsidekiq/README.txt "$D"/"$image_dir"/README.txt

# bundle it up
tar -C "$D" --owner=root --group=root -cvf "$image_dir".tar "$image_dir"
xz -9vf "$image_dir".tar
