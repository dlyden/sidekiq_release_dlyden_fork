#! /bin/bash

# exit on any error
set -o errexit

# Make sure the image date and SDK version are specified
if [ $# -ne 2 ]; then
    echo "usage: $(basename $(readlink -f $0)) <image_version> <SDK_version>" >&2
    echo >&2
    echo "    image_version is in the form YYYYMMDD" >&2
    echo "    SDK_version is in the form X.Y.Z" >&2
    echo >&2
    exit 1
fi

function copy-drivers()
{
    for model; do
        copy-all-released-drivers $D/$image_dir/driver jetson-"$model"
    done
}

D=$(dirname $(readlink -f $0))
source $D/shared-functions

image_dir=sidekiq_image_jetson_v$2_$1

# Create the image directory
rm -rf $D/$image_dir
mkdir -p $D/$image_dir

# copy driver, utils
tar -C $D/common -cf - driver utils | tar -C $D/$image_dir -xf -

# copy FPGA bitstreams
copy-fpga-bitstreams $D $D/$image_dir

# remove unsupported FPGA images
(cd $D/$image_dir/fpga;
 rm -f sidekiq_image_x4_htgk810*;
 rm -f sidekiq_image_x4_ams*;
 rm -f sidekiq_image_z2*.bi?;
 rm -f sidekiq_image_z3u_*.bi?)

# symlink remaining FPGA bitstreams
symlink-latest-fpga $D/$image_dir/fpga

# three different models, three different kernel configurations, the installer will need to
# differentiate during installation
copy-drivers tx1 tx2 xavier

# Copy the README.txt from sidekiq_sw submodule
cp $D/../sidekiq_sw/host_code/libsidekiq/README.txt $D/$image_dir/README.txt

# bundle it up
tar -C $D --owner=root --group=root -cvf $image_dir.tar $image_dir
xz -9vf $image_dir.tar
