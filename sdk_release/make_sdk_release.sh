#!/bin/bash

# This script is used to build a Sidekiq SDK release.  It creates a new
# stand-alone directory to hold the files that will go into the SDK
# release, and copies the needed files to the appropriate location in this
# new SDK release directory.

perform()
{
    echo "--> $@"
    "$@"
    local R=$?
    echo "--> $@ returned $R"
}

abort()
{
    echo $? >&2

    echo >&2 '***************'
    echo >&2 '*** ABORTED ***'
    echo >&2 '***************
'
    echo "An error occurred. Exiting..." >&2
    exit 1
}

# set up bash to call 'abort' function whenever a non-zero exit code is encountered
# * https://stackoverflow.com/a/22224317
# * https://vaneyckt.io/posts/safer_bash_scripts_with_set_euxo_pipefail/
trap 'abort' 0

if test -z "$1"
then
  echo "Error: no SDK version passed in (of the form Major.Minor.Patch, such as 0.4.0)"
  exit 1
fi

set -Eeuo pipefail

if ! type chrpath >/dev/null 2>&1; then
    echo "could not locate 'chrpath' or it is not executable" >&2
    echo "try 'sudo apt-get install chrpath' to install" >&2
    exit 1
fi

if ! type fpm >/dev/null 2>&1; then
    echo "could not locate 'fpm' or it is not executable" >&2
    echo "try:" >&2
    echo "  sudo apt-get install ruby-dev" >&2
    echo "  sudo gem install fpm" >&2
    exit 1
fi

if ! type rpm >/dev/null 2>&1; then
    echo "could not locate 'rpm' or it is not executable" >&2
    echo "try:" >&2
    echo "  sudo apt-get install rpm" >&2
    exit 1
fi

if ! type opkg-build >/dev/null 2>&1; then
    echo "could not locate 'opkg-build' or it is not executable" >&2
    echo "try:" >&2
    echo "  git clone /mnt/storage/gitrepo/opkg-utils.git" >&2
    echo "  cd opkg-utils" >&2
    echo "  sudo make install" >&2
    exit 1
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
TAR="tar --owner=root --group=root"

sdk_dir=$DIR
build_dir=$(readlink -f "$sdk_dir/../sidekiq_sw/")
doc_dir=$(readlink -f "$sdk_dir/../sidekiq_doc/")
api_doc_dir=$(readlink -f "$sdk_dir/../api_doc/")
libskiq_dir=$build_dir/host_code/libsidekiq
srfs_dir_default=$sdk_dir/srfs
srfs_dir=${srfs_dir:-${srfs_dir_default}}
srfs_dir=$(readlink -f "$srfs_dir")
test_apps_dir=$libskiq_dir/test_apps/
custom_xport_dir=$libskiq_dir/custom_xport_bare/

source_files=( app_src_file1 \
               fdd_rx_tx_samples \
               multicard_rx_samples \
               multicard_tx_samples \
               multicard_dynamic_enable \
               pps_tester \
               prog_fpga \
               read_accel \
               read_gpsdo \
               read_temp \
               rx_benchmark \
               rx_samples \
               rx_samples_freq_hopping \
               rx_samples_minimal \
               rx_samples_on_trigger \
               set_rx_LO_freq \
               sidekiq_probe \
               store_user_fpga \
               sweep_receive \
               tdd_rx_tx_samples \
               test_golden_present \
               tx_benchmark \
               tx_configure \
               tx_samples \
               tx_samples_async \
               tx_samples_on_1pps \
               tx_samples_from_FPGA_RAM \
               user_reg_test \
               version_test \
               xcv_benchmark )

source_files_no_prebuilt=( read_imu )

bin_files=( read_modules \
            ref_clock \
            test_sample_rate )

include_files=( sidekiq_core/inc/sidekiq_api.h \
                sidekiq_core/inc/sidekiq_types.h \
                sidekiq_core/inc/sidekiq_params.h \
                transport/inc/sidekiq_xport_api.h \
                transport/inc/sidekiq_xport_types.h )

BUILD_CONFIG=( x86_64.gcc9.4.0_glibc2.17 \
    x86_32.gcc4.8_glibc2.11 \
    arm_cortex-a9.gcc4.9.2_gnueabi \
    arm_cortex-a9.gcc5.2_glibc_openwrt \
    arm_cortex-a9.gcc4.8_gnueabihf_linux \
    arm_cortex-a9.gcc7.2.1_gnueabihf \
    aarch64.gcc6.3 )

PLATFORM=( msiq-sx-1602 \
           z2 \
           z2-armhf \
           z3u )

# the SDK release version is manually passed in on the cmd line for now
# (ideally this would be pulled from sidekiq_api.h...)
version=$1
echo "Creating Sidekiq SDK release directory structure for version $version"
dest_sdk_dir="$sdk_dir/sidekiq_sdk_v$version"
prebuilt_dir="$sdk_dir/sidekiq_sdk_v${version}_prebuilt_apps"
dest_sdk_dir_jetson="$sdk_dir/sidekiq_sdk_v${version}_jetson"
dest_sdk_dir_z3u="$sdk_dir/sidekiq_sdk_v${version}_z3u"
mkdir -p "$prebuilt_dir" "sidekiq_libraries_v$version"

function generate-documentation()
{
    # generate SDK manual
    (cd "$doc_dir"; ./make_documentation.sh "$version")

    # generate API documentation
    $DIR/../scripts/make-api-docs.sh "$version"
}

function create-dir-structure()
{
    local dest_sdk_dir=$1

    # setup SDK directory structure
    mkdir -p "$dest_sdk_dir"
    cd "$dest_sdk_dir"
    mkdir -p test_apps/src test_apps/bin lib prebuilt_apps

    # copy everything in test_apps over
    cp "$test_apps_dir/Makefile.for_sdk" ./test_apps/Makefile
    cp "$test_apps_dir/tools.mk" ./test_apps/
    for i in "${source_files[@]}" "${source_files_no_prebuilt[@]}"; do
        cp "$test_apps_dir/src/${i}.c" test_apps/src/
    done
    cp "$test_apps_dir/src/elapsed.h" ./test_apps/src/

    # copy everything in custom_xport_bare
    mkdir -p custom_xport_bare/src custom_xport_bare/test_apps/src
    (cd custom_xport_bare;
     cp "$custom_xport_dir/Makefile.for_sdk" Makefile;
     cp "$custom_xport_dir/tools.mk" .;
     cp -dR "$custom_xport_dir/src"/*.c src;
     cp -dR "$custom_xport_dir/test_apps/src"/*.c test_apps/src)

    # copy over inc and doc
    mkdir -p sidekiq_core sidekiq_core/inc arg_parser arg_parser/inc arg_parser/lib
    mkdir -p doc/manuals doc/sdk_manual doc/api
    for fn in "${include_files[@]}"; do
        cp "$libskiq_dir/$fn" sidekiq_core/inc/
    done

    cp "$libskiq_dir/../arg_parser/inc/arg_parser.h" arg_parser/inc/
    cp "$doc_dir/manuals"/*.pdf doc/manuals
    cp "$build_dir/host_code/libsidekiq/README.txt" .

    # copy SDK manual (HTML and PDF)
    mkdir -p ./doc/sdk_manual/html/
    cp -dR "$doc_dir/build/html" ./doc/sdk_manual/
    cp -f "$doc_dir/build/latex/sidekiq_software_development_manual.pdf" \
       "./doc/sdk_manual/Sidekiq_Software_Development_Manual_for_${version}.pdf"

    # copy API documentation (HTML and PDF)
    cp -dR "$api_doc_dir/html/" ./doc/api/
    cp -f "$api_doc_dir/latex/refman.pdf" "./doc/api/Sidekiq_API_${version}.pdf"
}


function build-tarxz()
{
    local NAME=$1
    shift

    $TAR -cf "${NAME}.tar" $@
    xz -9fev "${NAME}.tar"
}


function __build-pkg()
{
    local NAME=$1
    local VER=$2
    local ID=$3
    shift 3

    fpm -f -n "$NAME" -v "$VER" -s dir --verbose \
        -m 'Epiq Solutions <info@epiqsolutions.com>' \
        --vendor 'Epiq Solutions' \
        --url 'https://epiqsolutions.com/' \
        $@ -C "$ID" $(cd "$ID"; ls --hide=DEBIAN)
}

function build-rpm()
{
    __build-pkg $@ -t rpm --rpm-compression xz
}

function build-deb()
{
    __build-pkg $@ -t deb --deb-compression xz
}

function build-pkgs()
{
    build-rpm $@
    build-deb $@
}

function create-run-time-package()
{
    local ROOT=$1
    local BC=$2
    local ID=$3

    # convert BC to have no underscores
    local BCNU=$(echo "$BC" | tr '_' '-')

    mkdir -p "$ID"

    # copy all support libraries
    (cd "$ROOT/build.$BC/support" && find -name '*.so*' -print0 | xargs -0 tar -cf - | tar -C "$ID" -xf -)

    # move /usr/lib/lib64/* into /usr/lib/epiq
    if [ -e "$ID/usr/lib/lib64" ]; then
        mv "$ID/usr/lib/lib64"/* "$ID/usr/lib/epiq"
        rmdir "$ID/usr/lib/lib64"
    fi

    case $BC in
        arm_cortex-a9.gcc5.2_glibc_openwrt)
            # build opkg
            mkdir -p "$ID/CONTROL"
            DEPENDS="libc(>=2.21-1)"

            cat > "$ID/CONTROL/control" <<- EOF
Package: sidekiq-shared-libs-$BCNU
Priority: optional
Section: misc
Version: $version
Architecture: imx6
Maintainer: info@epiqsolutions.com
Source: none
Depends: $DEPENDS
Description: Shared libraries that are required by libsidekiq applications
EOF
            opkg-build -c -O "$ID"
            ;;

        arm_cortex-a9.gcc4.8_gnueabihf_linux|arm_cortex-a9.gcc4.9.2_gnueabi)
            # build tarball
            build-tarxz "sidekiq-shared-libs-${BC}_$version" -C "$ID" .
            ;;

        aarch64.gcc6.3)
            # build deb
            build-deb "sidekiq-shared-libs-${BC}" "$version" "$ID" -a arm64
            ;;

        x86_*)
            # build deb and rpm
            build-pkgs "sidekiq-shared-libs-${BC%9.4.0_glibc2.17}" "$version" "$ID"
            ;;
    esac
}



####################################################################################################
#
# build-srfs <srfs_dir> <BUILD_CONFIG=bc|PLATFORM=p> <build_dir>
#
# - build SRFS from source directory <srfs_dir> with build configuration
#     <BUILD_CONFIG=bc|PLATFORM=p> and use <build_dir> as the libsidekiq root directory.
#
function build-srfs()
{
    local srfs_dir=$1
    local build_string=$2
    local build_dir=$3
    shift 3

    perform make -C "$srfs_dir" \
            "$build_string" \
            USE_SIDEKIQ=y \
            TEST_APPS_STATIC=no \
            STRIP_ENABLED=enabled \
            CLONE_SIDEKIQ_ROOT="${build_dir}/" \
            BUILD_ROOT="${build_dir}/build" $@
}


################################################################################
#
# multi-cp </src/path/to/file> <dest/path/to/file> <dest1> [ <dest2> ... <destN> ]
#
# - copies the source file to multiple destinations at a relative path
#
function multi-cp()
{
    local SF=$1
    local DF=$2
    local dest
    shift 2

    for dest; do
        cp "${SF}" "${dest}/${DF}"
    done
}


################################################################################
#
# make-release <build> <build_release> <BUILD_CONFIG=bc|PLATFORM=p> <dest...>
#
function make-release()
{
    local build=$1
    local build_release=$2
    local build_string=$3
    local bin_output=$prebuilt_dir/${build_release}
    local dest needs_multi_applet
    local extra_prebuilt=()
    local skip_prebuilt=()
    shift 3

    mkdir -p "$bin_output"

    # build libsidekiq
    cd "$build_dir"
    perform make $build_string clean
    perform make $build_string TEST_APPS_STATIC=no STRIP_ENABLED=enabled -j8

    # build and/or include extra apps based on platform
    case "$build_string" in
        PLATFORM=msiq-sx-1602)
            build-srfs "$srfs_dir" "$build_string" "$build_dir" clean
            build-srfs "$srfs_dir" "$build_string" "$build_dir"

            perform cp "$srfs_dir/bin/srfs_rx__${build}" "$bin_output/srfs_rx"
            ;;
        PLATFORM=z2)
            extra_prebuilt=( read_imu )
            ;;
        PLATFORM=z2-armhf)
            extra_prebuilt=( read_imu \
                              skiq_daemon )
            ;;
        PLATFORM=z3u)
            extra_prebuilt=( skiq_daemon )
            skip_prebuilt=( multicard_rx_samples \
                            multicard_tx_samples \
                            multicard_dynamic_enable \
                            read_modules \
                            store_user_fpga \
                            test_golden_present \
                            tx_samples_async )
            ;;
    esac

    # copy over the sidekiq binaries (test_apps from the pre-built directory)
    needs_multi_applet=no
    for f in "${source_files[@]}" "${extra_prebuilt[@]+${extra_prebuilt[@]}}" "${bin_files[@]}"; do
        skip_app=no
        # iterate through the skip app list if it's set
        if [[ ${skip_prebuilt:+isset} ]]; then
            for skip in "${skip_prebuilt[@]}"; do
                if [ "$skip" == "$f" ]; then
                    skip_app=yes
                fi
            done
        fi

        if [ "$skip_app" == "no" ]; then 
            local src
            if [ -e "$test_apps_dir/bin/$build/$f" ]; then
                src="$test_apps_dir/bin/$build/$f"
            elif [ -e "$test_apps_dir/bin/${f}__${platform}" ]; then
                src=$test_apps_dir/bin/${f}__${platform}
            else
                echo "cannot find file '${f}'" >&2
                exit 1
            fi

            if [ -h "$src" ]; then
                ln -snf skiq_multi_call "$bin_output/$f"
                needs_multi_applet=yes
            else
                cp "$src" "$bin_output/$f"
            fi
        fi
    done

    # for those build configurations / platforms that have test apps that are symlinks, there's an
    # applet called 'skiq_multi_call' that needs to be copied as well
    if [[ $needs_multi_applet == "yes" ]]; then
        local f=skiq_multi_call
        if [ -e "$test_apps_dir/bin/$build/$f" ]; then
            cp "$test_apps_dir/bin/$build/$f" "$bin_output/$f"
        elif [ -e "$test_apps_dir/bin/${f}__${platform}" ]; then
            cp "$test_apps_dir/bin/${f}__${platform}" "$bin_output/$f"
        else
            echo "cannot find file '${f}'" >&2
            exit 1
        fi
    fi

    # copy over the sidekiq library
    multi-cp build.$build/lib/libsidekiq__$build.a "lib/libsidekiq__${build_release}.a" "$@"

    # copy over the arg parser lib
    multi-cp "$build_dir/host_code/arg_parser/arg_parser__${build}.a" "arg_parser/lib/arg_parser__${build_release}.a" "$@"

    # copy over the support libraries / headers (glib, etc)
    for dest; do
        mkdir -p "$dest/lib/support/${build_release}"
        cp -R build.$build/support/* "$dest/lib/support/${build_release}/"
    done

    # create run-time structure
    cd "$sdk_dir"
    create-run-time-package "$build_dir" $build "$sdk_dir/libs-${build_release}"

    cd "$sdk_dir"
    build-tarxz "sidekiq_sdk_v${version}_prebuilt_apps__${build_release}" \
                "sidekiq_sdk_v${version}_prebuilt_apps/${build_release}"
}

generate-documentation
create-dir-structure "$dest_sdk_dir"
create-dir-structure "$dest_sdk_dir_jetson"
create-dir-structure "$dest_sdk_dir_z3u"

# compile for each platform
for build in "${BUILD_CONFIG[@]}"; do
    # We do some extra mapping here to translate the actual BUILD_CONFIG used
    # for compilation to that which is presented to the customer for the
    # release. The main reason for this is for the x86 builds which are
    # compiled using a version of GCC that supports older GLIBC versions found
    # on CentOS releases.
    case $build in
        x86_32.gcc4.8_glibc2.11)
            build_release=${build%"4.8_glibc2.11"}
            ;;
        x86_64.gcc9.4.0_glibc2.17)
            build_release=${build%"9.4.0_glibc2.17"}
            ;;
        *)
            build_release=${build}
            ;;
    esac

    case $build in
        aarch64.gcc6.3)
            make-release "$build" "$build_release" BUILD_CONFIG=$build $dest_sdk_dir $dest_sdk_dir_jetson
            make-release "$build" "$build_release" BUILD_CONFIG=$build $dest_sdk_dir $dest_sdk_dir_z3u
            ;;
        *)
            make-release "$build" "$build_release" BUILD_CONFIG=$build $dest_sdk_dir
            ;;
    esac
done

for platform in "${PLATFORM[@]}"; do
    # We do some extra mapping here to translate the actual PLATFORM used for compilation to that
    # which is presented to the customer for the release.  The customer is presented 'msiq-sx' as a
    # PLATFORM, but we need to translate our internal 'msiq-sx-1602' PLATFORM.
    case $platform in
        msiq-sx-1602)
            platform_release=${platform%"-1602"}
            ;;
        *)
            platform_release=${platform}
            ;;
    esac

    make-release "$platform" "$platform_release" PLATFORM=$platform $dest_sdk_dir
done

# copy the x86_64.gcc prebuilt apps
cp -r "$prebuilt_dir/x86_64.gcc" "$dest_sdk_dir/prebuilt_apps/"

# copy the aarch64.gcc6.3 prebuilt apps
cp -r "$prebuilt_dir/aarch64.gcc6.3" "$dest_sdk_dir_jetson/prebuilt_apps/"

# copy the Z3u prebuilt apps
cp -r "$prebuilt_dir/z3u" "$dest_sdk_dir_z3u/prebuilt_apps/"

cp sidekiq-shared*"$version"* "sidekiq_libraries_v$version/"

echo "Done building SDK release directory structure"
cd "$sdk_dir"

for tb in sidekiq_sdk_v$version sidekiq_sdk_v${version}_jetson sidekiq_sdk_v${version}_z3u; do
    echo "Creating ${tb} release tarball ... can take 2-3 minutes"
    build-tarxz "${tb}" "${tb}"
    echo "Done building ${tb} release tarball"
done

# unregister the trap
trap : 0
