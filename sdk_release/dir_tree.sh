#! /bin/bash

if true; then
    tree -d -L 3 --charset=ascii sidekiq_sdk_v4.2.0
    tree -a -L 3 --charset=ascii sidekiq_sdk_v4.2.0/arg_parser/
    tree -a -L 3 --charset=ascii sidekiq_sdk_v4.2.0/custom_xport_bare/
    tree -a -L 1 --charset=ascii sidekiq_sdk_v4.2.0/doc/
    tree -a -L 2 --charset=ascii sidekiq_sdk_v4.2.0/lib/
    tree -a -L 2 --charset=ascii sidekiq_sdk_v4.2.0/prebuilt_apps/
    tree -a -L 2 --charset=ascii sidekiq_sdk_v4.2.0/sidekiq_core/
    tree -a -L 2 --charset=ascii sidekiq_sdk_v4.2.0/test_apps/
fi | \
    grep -v '^[0-9]'
