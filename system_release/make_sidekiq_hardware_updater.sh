#! /bin/bash

if [ $# -ne 2 ]; then
    echo "usage: $(basename $(readlink -f $0)) <YYYYMMDD> <X.Y.Z>" >&2
    echo "       where YYYYMMDD is the image version" >&2
    echo "       where X.Y.Z is the SDK version" >&2
    exit 1
fi

IMAGE=$1
SDK=$2
DEST=sidekiq_hardware_updater
BCs="x86_64.gcc arm_cortex-a9.gcc5.2_glibc_openwrt aarch64.gcc6.3"
POST_EXTRACT=sidekiq_hardware_updater.sh
TAR="tar --owner=root:0 --group=root:0"

# exit on error and errors with pipelines
set -o errexit
set -o pipefail

# tries both version types, then picks semantic if available or falls back to
# traditional
function extract-version()
{
    local SEM=$(extract-semantic-version "$@")
    local TRAD=$(extract-traditional-version "$@")
    if [ ! -z "$SEM" ]; then
        echo "$SEM"
    else
        echo "$TRAD"
    fi
}

# extracts the traditional X.Y version
function extract-traditional-version()
{
    set +o pipefail
    echo "$@" | \
        sort -g | \
        tail -n1 | \
        grep -oP '\d+\.\d+' | \
        grep -oP '\d+'
    set -o pipefail
}

# extracts the semantic X.Y.Z version
function extract-semantic-version()
{
    set +o pipefail
    echo "$@" | \
        sort -g | \
        tail -n1 | \
        grep -oP '\d+\.\d+\.\d+' | \
        grep -oP '\d+'
    set -o pipefail
}

function copy-file-here()
{
    for FN; do
        if [ ! -e $FN ]; then
            echo " - required file '$FN' is missing, quitting" >&2
            exit 1
        else
            echo "copying required file '$FN'" >&2
            mkdir -p $DEST
            cp -aH $FN $DEST/
        fi
    done
}

function copy-dir-here()
{
    for D; do
        if [ ! -e $D ]; then
            echo " - required directory '$D' is missing, quitting" >&2
            exit 1
        else
            echo "copying required directory '$D'" >&2
            mkdir -p $DEST
            cp -a $D $DEST/
        fi
    done
}

function strip-script()
{
    local FN=$1
    local MKT=$(mktemp)

    # transmogrify the script (strip out whitespace / comments)
    # http://stackoverflow.com/questions/25291228/how-to-remove-comments-from-a-bash-script
    env "BASH_FUNC_tmp_%%=() {
          $(<$FN)
         }" bash -c 'declare -f tmp_' | \
    tail -n+3 | \
    head -n-1 | \
    cut -b 5- > $MKT

    (echo "#! /bin/bash"; cat $MKT) > $FN
    rm -f $MKT
}

function join()
{
    local IFS="$1"
    shift
    echo "$*"
}

echo "removing firmware, FPGA images, and updater test apps"
rm -f ${DEST}/sidekiq_fw_{m2,mpcie}_v*.*.hex
rm -f ${DEST}/sidekiq_image_{m2,mpcie,x2,x2_xcku115,m2_2280,nv100}_xport_pcie_*.*.bi?
rm -f ${DEST}/sidekiq_image_{x4,x4_xcku115,x4_htgk810_xcku060,x4_htgk810_xcku115}_xport_pcie_*.*.bi?
rm -rf ${DEST}/update_test_apps*
rm -f ${DEST}/${POST_EXTRACT}

# copy firmware and FPGA images
copy-file-here $(ls ../sidekiq_fw/sidekiq_fw_{m2,mpcie}_v*.*.hex | grep -v _factory)
copy-file-here ../image_release/common/fpga/sidekiq_image_{m2,mpcie,x2,x2_xcku115,m2_2280,nv100}_xport_pcie_*.bi?
copy-file-here ../image_release/common/fpga/sidekiq_image_{x4,x4_xcku115,x4_htgk810_xcku060,x4_htgk810_xcku115}_xport_pcie_*.bi?

# there are a ton of x86-64 kernel modules, so unpackage them from the image (where they are
# separated by distribution) and repackage them into a single tarball.  during the updater, we can
# extract just the modules we need
mkdir -p ${DEST}/driver
cp ../image_release/sidekiq_image_v${SDK}_${IMAGE}/driver/load_sidekiq_drivers.sh ${DEST}/driver

mkdir -p ${DEST}/driver_tmp
for fn in ../image_release/sidekiq_image_v${SDK}_${IMAGE}/driver/*.tar.xz; do
    tar -C ${DEST}/driver_tmp -xf $fn
done
eval $TAR -C ${DEST}/driver_tmp -cf ${DEST}/drivers.tar .
xz -9fev ${DEST}/drivers.tar
rm -rf ${DEST}/driver_tmp

# copy all DMA driver and PCI manager kernel modules
# note: the order of the next two copies matters.  Jetson must happen after openwrt since
# openwrt uses a modified load_sidekiq_drivers and will clobber the other version.

copy-dir-here ../image_release/sidekiq_image_openwrt_v${SDK}_${IMAGE}/root/sidekiq_image_current/driver
copy-dir-here ../image_release/sidekiq_image_jetson_v${SDK}_${IMAGE}/driver

# get the firmware versions
MPCIE_FW_VERS=($(extract-version ${DEST}/sidekiq_fw_mpcie_v*.*.hex))
M2_FW_VERS=($(extract-version ${DEST}/sidekiq_fw_m2_v*.*.hex))

# get the FPGA versions
MPCIE_FPGA_VERS=($(extract-version ${DEST}/sidekiq_image_mpcie_xport_pcie_*.*.bi?))
M2_FPGA_VERS=($(extract-version ${DEST}/sidekiq_image_m2_xport_pcie_*.*.bi?))
X2_FPGA_VERS=($(extract-version ${DEST}/sidekiq_image_x2_xport_pcie_*.*.bi?))
X2_XCKU115_FPGA_VERS=($(extract-version ${DEST}/sidekiq_image_x2_xcku115_xport_pcie_*.*.bi?))
X4_FPGA_VERS=($(extract-version ${DEST}/sidekiq_image_x4_xport_pcie_*.*.bi?))
X4_XCKU115_FPGA_VERS=($(extract-version ${DEST}/sidekiq_image_x4_xcku115_xport_pcie_*.*.bi?))
X4_HTGK810_XCKU060_FPGA_VERS=($(extract-version ${DEST}/sidekiq_image_x4_htgk810_xcku060_xport_pcie_*.*.bi?))
X4_HTGK810_XCKU115_FPGA_VERS=($(extract-version ${DEST}/sidekiq_image_x4_htgk810_xcku115_xport_pcie_*.*.bi?))
M2_2280_FPGA_VERS=($(extract-version ${DEST}/sidekiq_image_m2_2280_xport_pcie_*.*.bi?))
NV100_FPGA_VERS=($(extract-version ${DEST}/sidekiq_image_nv100_xport_pcie_*.*.bi?))

# copy test apps that are used in updating
for BC in $BCs; do
    case $BC in
        # use the apps built using the CentOS toolchain for compatibility
        x86_32.gcc)
            suffix="4.8_glibc2.11"
            build=${BC}${suffix}
            ;;
        x86_64.gcc)
            suffix="9.4.0_glibc2.17"
            build=${BC}${suffix}
            ;;
        *)
            build=${BC}
            ;;
    esac

    mkdir -p ${DEST}/update_test_apps_${BC}
    echo "copying test apps:"
    tar -C ../sidekiq_sw/host_code/libsidekiq/test_apps/bin/${build} -cf - \
        card_mgr_uninit \
        fx2_reprogram \
        prog_fpga \
        ref_clock_stamp \
        sidekiq_probe \
        store_golden_fpga \
        store_user_fpga \
        test_firmware_vers \
        test_fpga_vers \
        test_genuine_golden_present \
        test_hardware_vers \
        | tar -C ${DEST}/update_test_apps_${BC} -xvf - | sed -e 's,^,\t,'

    echo "copying shared libraries:"
    (cd ../sdk_release/libs-${BC}/usr/lib/epiq; find -name 'lib*.so*') | \
        xargs tar -C ../sdk_release/libs-${BC}/usr/lib/epiq -cf - | \
        tar -C ${DEST}/update_test_apps_${BC} -xvf - | sed -e 's,^./,\t,;s,^[^\t],\t,'

    echo "compressing test apps and shared libraries for $BC:"
    eval $TAR -C ${DEST}/update_test_apps_${BC} -cf ${DEST}/update_test_apps_${BC}.tar .
    xz -9fev ${DEST}/update_test_apps_${BC}.tar
    rm -rf ${DEST}/update_test_apps_${BC}
done

# order is important here so variables that are substrings of other variables
# don't get clobbered
cp -f post_extract/${POST_EXTRACT}.template ${DEST}/${POST_EXTRACT}
sed -i -e "s,%M2_FW_VERSION%,$(join . ${M2_FW_VERS[@]}),g" ${DEST}/${POST_EXTRACT}
sed -i -e "s,%M2_FW_IMAGE%,$(basename ./${DEST}/sidekiq_fw_m2_v*.*.hex),g" ${DEST}/${POST_EXTRACT}

sed -i -e "s,%X2_FPGA_VERSION%,$(join . ${X2_FPGA_VERS[@]}),g" ${DEST}/${POST_EXTRACT}
sed -i -e "s,%X2_FPGA_VERSION_SPACE%,$(join ' ' ${X2_FPGA_VERS[@]}),g" ${DEST}/${POST_EXTRACT}
sed -i -e "s,%X2_FPGA_IMAGE%,$(basename ./${DEST}/sidekiq_image_x2_xport_pcie_*.bi?),g" ${DEST}/${POST_EXTRACT}

sed -i -e "s,%X2_HTGK800_XCKU115_FPGA_VERSION%,$(join . ${X2_XCKU115_FPGA_VERS[@]}),g" ${DEST}/${POST_EXTRACT}
sed -i -e "s,%X2_HTGK800_XCKU115_FPGA_VERSION_SPACE%,$(join ' ' ${X2_XCKU115_FPGA_VERS[@]}),g" ${DEST}/${POST_EXTRACT}
sed -i -e "s,%X2_HTGK800_XCKU115_FPGA_IMAGE%,$(basename ./${DEST}/sidekiq_image_x2_xcku115_xport_pcie_*.bi?),g" ${DEST}/${POST_EXTRACT}

sed -i -e "s,%X4_HTGK800_XCKU060_FPGA_VERSION%,$(join . ${X4_FPGA_VERS[@]}),g" ${DEST}/${POST_EXTRACT}
sed -i -e "s,%X4_HTGK800_XCKU060_FPGA_VERSION_SPACE%,$(join ' ' ${X4_FPGA_VERS[@]}),g" ${DEST}/${POST_EXTRACT}
sed -i -e "s,%X4_HTGK800_XCKU060_FPGA_IMAGE%,$(basename ./${DEST}/sidekiq_image_x4_xport_pcie_*.bi?),g" ${DEST}/${POST_EXTRACT}

sed -i -e "s,%X4_HTGK800_XCKU115_FPGA_VERSION%,$(join . ${X4_XCKU115_FPGA_VERS[@]}),g" ${DEST}/${POST_EXTRACT}
sed -i -e "s,%X4_HTGK800_XCKU115_FPGA_VERSION_SPACE%,$(join ' ' ${X4_XCKU115_FPGA_VERS[@]}),g" ${DEST}/${POST_EXTRACT}
sed -i -e "s,%X4_HTGK800_XCKU115_FPGA_IMAGE%,$(basename ./${DEST}/sidekiq_image_x4_xcku115_xport_pcie_*.bi?),g" ${DEST}/${POST_EXTRACT}

sed -i -e "s,%X4_HTGK810_XCKU060_FPGA_VERSION%,$(join . ${X4_HTGK810_XCKU060_FPGA_VERS[@]}),g" ${DEST}/${POST_EXTRACT}
sed -i -e "s,%X4_HTGK810_XCKU060_FPGA_VERSION_SPACE%,$(join ' ' ${X4_HTGK810_XCKU060_FPGA_VERS[@]}),g" ${DEST}/${POST_EXTRACT}
sed -i -e "s,%X4_HTGK810_XCKU060_FPGA_IMAGE%,$(basename ./${DEST}/sidekiq_image_x4_htgk810_xcku060_xport_pcie_*.bi?),g" ${DEST}/${POST_EXTRACT}

sed -i -e "s,%X4_HTGK810_XCKU115_FPGA_VERSION%,$(join . ${X4_HTGK810_XCKU115_FPGA_VERS[@]}),g" ${DEST}/${POST_EXTRACT}
sed -i -e "s,%X4_HTGK810_XCKU115_FPGA_VERSION_SPACE%,$(join ' ' ${X4_HTGK810_XCKU115_FPGA_VERS[@]}),g" ${DEST}/${POST_EXTRACT}
sed -i -e "s,%X4_HTGK810_XCKU115_FPGA_IMAGE%,$(basename ./${DEST}/sidekiq_image_x4_htgk810_xcku115_xport_pcie_*.bi?),g" ${DEST}/${POST_EXTRACT}

sed -i -e "s,%MPCIE_FW_VERSION%,$(join . ${MPCIE_FW_VERS[@]}),g" ${DEST}/${POST_EXTRACT}
sed -i -e "s,%MPCIE_FW_IMAGE%,$(basename ./${DEST}/sidekiq_fw_mpcie_v*.*.hex),g" ${DEST}/${POST_EXTRACT}

sed -i -e "s,%MPCIE_FPGA_VERSION%,$(join . ${MPCIE_FPGA_VERS[@]}),g" ${DEST}/${POST_EXTRACT}
sed -i -e "s,%MPCIE_FPGA_VERSION_SPACE%,$(join ' ' ${MPCIE_FPGA_VERS[@]}),g" ${DEST}/${POST_EXTRACT}
sed -i -e "s,%MPCIE_FPGA_IMAGE%,$(basename ./${DEST}/sidekiq_image_mpcie_xport_pcie_*.bi?),g" ${DEST}/${POST_EXTRACT}

sed -i -e "s,%M2_FPGA_VERSION%,$(join . ${M2_FPGA_VERS[@]}),g" ${DEST}/${POST_EXTRACT}
sed -i -e "s,%M2_FPGA_VERSION_SPACE%,$(join ' ' ${M2_FPGA_VERS[@]}),g" ${DEST}/${POST_EXTRACT}
sed -i -e "s,%M2_FPGA_IMAGE%,$(basename ./${DEST}/sidekiq_image_m2_xport_pcie_*.bi?),g" ${DEST}/${POST_EXTRACT}

sed -i -e "s,%M2_2280_FPGA_VERSION%,$(join . ${M2_2280_FPGA_VERS[@]}),g" ${DEST}/${POST_EXTRACT}
sed -i -e "s,%M2_2280_FPGA_VERSION_SPACE%,$(join ' ' ${M2_2280_FPGA_VERS[@]}),g" ${DEST}/${POST_EXTRACT}
sed -i -e "s,%M2_2280_FPGA_IMAGE%,$(basename ./${DEST}/sidekiq_image_m2_2280_xport_pcie_*.bi?),g" ${DEST}/${POST_EXTRACT}

sed -i -e "s,%NV100_FPGA_VERSION%,$(join . ${NV100_FPGA_VERS[@]}),g" ${DEST}/${POST_EXTRACT}
sed -i -e "s,%NV100_FPGA_VERSION_SPACE%,$(join ' ' ${NV100_FPGA_VERS[@]}),g" ${DEST}/${POST_EXTRACT}
sed -i -e "s,%NV100_FPGA_IMAGE%,$(basename ./${DEST}/sidekiq_image_nv100_xport_pcie_*.bi?),g" ${DEST}/${POST_EXTRACT}

# compress the FPGA bitstreams
xz -fv ./${DEST}/sidekiq_image_*_xport_*.bi?

strip-script ${DEST}/${POST_EXTRACT}
chmod a+x ${DEST}/${POST_EXTRACT}
fakeroot makeself \
      --header ./makeself-header.sh \
      --nox11 --xz \
      ${DEST} \
      sidekiq_hardware_updater_for_v${SDK}.sh \
      "Sidekiq Hardware Updater (v${SDK})" \
      ./${POST_EXTRACT}

echo
echo "Sidekiq Hardware Updater size summary:"
echo -n "unpacked: "; du -sh ${DEST}
echo -n "packed:   "; du -sh sidekiq_hardware_updater_for_v${SDK}.sh
