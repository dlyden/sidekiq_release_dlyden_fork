#!/bin/sh

###########################################################################
# m.2 version info - update for every release
m2_fpga_date=0x22090920
m2_fpga_hash=0x0dd13ebc

# mPCIe version info - update for every release
mpcie_fpga_date=0x20060518
mpcie_fpga_hash=0x0a4725c4

# X2 version info - update for every release
x2_fpga_date=0x22021508
x2_fpga_hash=0x06d70e43

# X2 HTG-K800 XCKU115 version info - update for every release
x2_htgk800_xcku115_fpga_date=0x22021500
x2_htgk800_xcku115_fpga_hash=0x06d70e43

# X4 HTG-K800 XCKU060 version info - update for every release
x4_htgk800_xcku060_fpga_date=0x23012617
x4_htgk800_xcku060_fpga_hash=0x0c168bcf

# X4 HTG-K800 XCKU115 version info - update for every release
x4_htgk800_xcku115_fpga_date=0x23012618
x4_htgk800_xcku115_fpga_hash=0x0c168bcf

# X4 HTG-K810 XCKU060 version info - update for every release
x4_htgk810_xcku060_fpga_date=0x23012621
x4_htgk810_xcku060_fpga_hash=0x0c168bcf

# X4 HTG-K810 XCKU115 version info - update for every release
x4_htgk810_xcku115_fpga_date=0x23012622
x4_htgk810_xcku115_fpga_hash=0x0c168bcf

# M.2-2280 version info - update for every release
m2_2280_fpga_date=0x22090919
m2_2280_fpga_hash=0x0dd13ebc

# NV100 version info - update for every release
nv100_fpga_date=0x23022813
nv100_fpga_hash=0x0d09f605
###########################################################################


###########################################################################
# m.2 updates
m2_golden_fpga_image=golden/sidekiq_image_m2_xport_pcie_1.255.bit

# mPCIe updates
mpcie_golden_fpga_image=golden/sidekiq_image_mpcie_xport_pcie_1.255.bin

# x2 updates
x2_golden_fpga_image=golden/sidekiq_image_x2_xport_pcie_1.255.bin

# x2 HTG-K800 XCKU115 updates
x2_htgk800_xcku115_golden_fpga_image=golden/sidekiq_image_x4_xcku115_xport_pcie_1.255.bin

# x4 HTG-K800 XCKU060 updates - X4 uses the same golden bitstream as X2
x4_htgk800_xcku060_golden_fpga_image=golden/sidekiq_image_x2_xport_pcie_1.255.bin

# x4 HTG-K800 XCKU115 updates
x4_htgk800_xcku115_golden_fpga_image=golden/sidekiq_image_x4_xcku115_xport_pcie_1.255.bin

# x4 HTG-K810 XCKU060 updates
x4_htgk810_xcku060_golden_fpga_image=golden/sidekiq_x4_htgk810_xcku060_golden_20101512_1a4cc87b.bin

# x4 HTG-K810 XCKU115 updates
x4_htgk810_xcku115_golden_fpga_image=golden/sidekiq_x4_htgk810_xcku115_golden_20101513_1a4cc87b.bin

# M.2-2280 updates
m2_2280_golden_fpga_image=golden/sidekiq_image_m2_2280_xport_pcie_1.255.bin

# NV100 updates
nv100_golden_fpga_image=golden/sidekiq_image_nv100_xport_pcie_1.255.bin
###########################################################################


###########################################################################
# function: display_help
#
display_help()
{
    echo "usage: sidekiq_updater.sh all | <SN> [ <SN> ... ]" >&2
    echo >&2
    echo "       all                Update all Sidekiq cards in the system" >&2
    echo "       <SN> [ <SN> ... ]  Update Sidekiq cards by serial number" >&2
    echo >&2
    echo "       This updater contains the following versions:" >&2
    echo "                mPCIe: FPGA %MPCIE_FPGA_VERSION%, build date $mpcie_fpga_date, hash $mpcie_fpga_hash, firmware %MPCIE_FW_VERSION%" >&2
    echo "                  m.2: FPGA %M2_FPGA_VERSION%, build date $m2_fpga_date, hash $m2_fpga_hash, firmware %M2_FW_VERSION%" >&2
    echo "                   X2: FPGA %X2_FPGA_VERSION%, build date $x2_fpga_date, hash $x2_fpga_hash" >&2
    echo "           X2 (KU115): FPGA %X2_HTGK800_XCKU115_FPGA_VERSION%, build date $x2_htgk800_xcku115_fpga_date, hash $x2_htgk800_xcku115_fpga_hash" >&2
    echo "                   X4: FPGA %X4_HTGK800_XCKU060_FPGA_VERSION%, build date $x4_htgk800_xcku060_fpga_date, hash $x4_htgk800_xcku060_fpga_hash" >&2
    echo "           X4 (KU115): FPGA %X4_HTGK800_XCKU115_FPGA_VERSION%, build date $x4_htgk800_xcku115_fpga_date, hash $x4_htgk800_xcku115_fpga_hash" >&2
    echo "  X4 HTG-K810 (KU060): FPGA %X4_HTGK810_XCKU060_FPGA_VERSION%, build date $x4_htgk810_xcku060_fpga_date, hash $x4_htgk810_xcku060_fpga_hash" >&2
    echo "  X4 HTG-K810 (KU115): FPGA %X4_HTGK810_XCKU115_FPGA_VERSION%, build date $x4_htgk810_xcku115_fpga_date, hash $x4_htgk810_xcku115_fpga_hash" >&2
    echo "             M.2-2280: FPGA %M2_2280_FPGA_VERSION%, build date $m2_2280_fpga_date, hash $m2_2280_fpga_hash" >&2
    echo "                NV100: FPGA %NV100_FPGA_VERSION%, build date $nv100_fpga_date, hash $nv100_fpga_hash" >&2
    echo >&2
}


###########################################################################
# function: banner <text...>
#
banner()
{
    echo " $@ " | sed -e :a -e 's/^.\{1,80\}$/- & -/;ta'
}

###########################################################################
# function: major <M.N>
#
# echoes M
#
major()
{
    echo "$1" | cut -d. -f1
}

###########################################################################
# function: minor <M.N>
#
# echoes N
#
minor()
{
    echo "$1" | cut -d. -f2
}


###########################################################################
# function: my_readlink <FILE>
#
# canonicalizes FILE path, even with busybox's readlink
#
my_readlink()
{
    local A

    A=$(readlink -f "$1")

    # with a busybox version of readlink, if the destination doesn't exist, it provides the empty
    # string :(, so check for that and correct it
    if [ -z "$A" ]; then
        A=$(readlink -f "$(pwd)")/$1
    fi

    echo "$A"
}


###########################################################################
# function: version_eq <M.N> <S.T>
#
# returns 0 if M.N is equal to S.T
# returns 1 otherwise
#
version_eq()
{
    local A="$1"
    local B="$2"

    if [ $(major $A) -eq $(major $B) -a $(minor $A) -eq $(minor $B) ]; then
        return 0
    else
        return 1
    fi
}


###########################################################################
# function: version_ge <M.N> <S.T>
#
# returns 0 if M.N is greater than or equal to S.T
# returns 1 otherwise
#
version_ge()
{
    local A="$1"
    local B="$2"

    if [ $(major $A) -gt $(major $B) ]; then
        return 0
    elif [ $(major $A) -eq $(major $B) -a $(minor $A) -ge $(minor $B) ]; then
        return 0
    else
        return 1
    fi
}


###########################################################################
# function: update_golden_fpga_if_needed <sernum> <image>
#
update_golden_fpga_if_needed()
{
    local SN=$1
    local GOLDEN=$2

    ###########################################################################
    # Test and update the golden
    $UTA/test_genuine_golden_present --serial $SN
    UPDATE=$?
    case $UPDATE in
        0)
            banner "Golden FPGA image detected on $SN"
            ;;
        1|2)
            ###########################################################################
            # decompress GOLDEN.xz, but if we're going to use it
            if [ ! -e $GOLDEN ]; then
                if [ -e ${GOLDEN}.xz ]; then
                    banner "Decompressing FPGA (golden) bitstream"
                    xz -dc ${GOLDEN}.xz > ${GOLDEN}
                else
                    banner "Cannot locate FPGA golden bitstream for Sidekiq s/n $SN, skipping"
                    return
                fi
            fi

            banner "Storing golden FPGA on $SN"
            $UTA/store_golden_fpga --verbose --verify --serial $SN -s $GOLDEN
            ;;
        *)
            banner "Error determining golden FPGA presence on $SN, cannot proceed"
            FAIL=1
            ;;
    esac
}


###########################################################################
# function: update_fpga_if_needed <sernum> <datestamp> <major> <minor> <patch> <hash> <image> [REPROG]
#
update_fpga_if_needed()
{
    local SN=$1
    local DATE=$2
    local MAJOR=$3
    local MINOR=$4
    local PATCH=$5
    local HASH=$6
    local IMAGE=$7              # IMAGE is assumed to be an absolute path to the file
    local REPROG=$8
    REPROG=${REPROG:-yes}

    ###########################################################################
    # update FPGA if necessary
    $UTA/test_fpga_vers --serial $SN -b $DATE --major $MAJOR --minor $MINOR --patch $PATCH -g $HASH
    UPDATE=$?

    ###########################################################################
    # decompress IMAGE.xz, but if we're going to use it
    if [ $UPDATE -ne 0 -o x"$REPROG" = x"yes" ]; then
        if [ ! -e $IMAGE ]; then
            if [ -e ${IMAGE}.xz ]; then
                banner "Decompressing FPGA bitstream"
                xz -d ${IMAGE}.xz
            else
                banner "Cannot locate FPGA bitstream for Sidekiq s/n $SN, skipping"
                return
            fi
        fi
    fi

    if [ x"$REPROG" = x"yes" ]; then
        ###########################################################################
        # program the FPGA, solves the case where testing the version went wrong
        $UTA/prog_fpga --serial $SN --source $IMAGE 2>/dev/null
    fi

    ###########################################################################
    # update FPGA if necessary
    if [ $UPDATE -eq 1 ] ; then
        banner "Storing user FPGA on $SN"
        $UTA/store_user_fpga --verbose --verify --serial $SN -s $IMAGE --reload
    elif [ $UPDATE -eq 0 ] ; then
        banner "No FPGA update required on $SN"
    else
        banner "Error retrieving FPGA version on $SN, attempting to force programming over USB"
        $UTA/store_user_fpga --verbose --verify --serial $SN -s $IMAGE
    fi
}


###########################################################################
# function: update_x4_fpga_if_needed <sernum> <fpga> <fmc> [REPROG]
#
update_x4_fpga_if_needed()
{
    local SN FPGA FMC
    SN=$1
    FPGA=$2
    FMC=$3

    local DATE VERSION HASH IMAGE GOLDEN

    case ${FPGA}_${FMC} in
        xcku060_htg_k800)
            DATE=$x4_htgk800_xcku060_fpga_date
            HASH=$x4_htgk800_xcku060_fpga_hash
            GOLDEN=$x4_htgk800_xcku060_golden_fpga_image
            VERSION="%X4_HTGK800_XCKU060_FPGA_VERSION_SPACE%"
            IMAGE=$(my_readlink %X4_HTGK800_XCKU060_FPGA_IMAGE%)
            ;;
        xcku115_htg_k800)
            DATE=$x4_htgk800_xcku115_fpga_date
            HASH=$x4_htgk800_xcku115_fpga_hash
            GOLDEN=$x4_htgk800_xcku115_golden_fpga_image
            VERSION="%X4_HTGK800_XCKU115_FPGA_VERSION_SPACE%"
            IMAGE=$(my_readlink %X4_HTGK800_XCKU115_FPGA_IMAGE%)
            ;;
        xcku060_htg_k810)
            DATE=$x4_htgk810_xcku060_fpga_date
            HASH=$x4_htgk810_xcku060_fpga_hash
            GOLDEN=$x4_htgk810_xcku060_golden_fpga_image
            VERSION="%X4_HTGK810_XCKU060_FPGA_VERSION_SPACE%"
            IMAGE=$(my_readlink %X4_HTGK810_XCKU060_FPGA_IMAGE%)
            ;;
        xcku115_htg_k810)
            DATE=$x4_htgk810_xcku115_fpga_date
            HASH=$x4_htgk810_xcku115_fpga_hash
            GOLDEN=$x4_htgk810_xcku115_golden_fpga_image
            VERSION="%X4_HTGK810_XCKU115_FPGA_VERSION_SPACE%"
            IMAGE=$(my_readlink %X4_HTGK810_XCKU115_FPGA_IMAGE%)
            ;;
        *)
            banner "!!! This Sidekiq Updater does not support the FPGA device ($FPGA) on FMC carrier ($FMC) for Sidekiq X4 (s/n $SN)"
            return 0
            ;;
    esac

    FAIL=0
    if [ -n "$GOLDEN" ]; then
        update_golden_fpga_if_needed $SN $(my_readlink $GOLDEN)
    fi

    # when FAIL is non-zero, card may not have golden FPGA, play it safe
    if [ $FAIL -eq 0 ]; then
        update_fpga_if_needed $SN $DATE $VERSION $HASH $IMAGE no
    else
        FAILURES=$((FAILURES+1))
    fi
}


###########################################################################
# function: update_nv100_card <sernum>
#
update_nv100_card()
{
    local SN=$1

    FAIL=0
    update_golden_fpga_if_needed $SN $(my_readlink $nv100_golden_fpga_image)

    # when FAIL is non-zero, card may not have golden FPGA, play it safe
    if [ $FAIL -eq 0 ]; then
        update_fpga_if_needed $SN $nv100_fpga_date %NV100_FPGA_VERSION_SPACE% $nv100_fpga_hash $(my_readlink %NV100_FPGA_IMAGE%) no
    else
        FAILURES=$((FAILURES+1))
    fi
}


###########################################################################
# function: update_m2_2280_card <sernum>
#
update_m2_2280_card()
{
    local SN=$1

    FAIL=0
    update_golden_fpga_if_needed $SN $(my_readlink $m2_2280_golden_fpga_image)

    # when FAIL is non-zero, card may not have golden FPGA, play it safe
    if [ $FAIL -eq 0 ]; then
        update_fpga_if_needed $SN $m2_2280_fpga_date %M2_2280_FPGA_VERSION_SPACE% $m2_2280_fpga_hash $(my_readlink %M2_2280_FPGA_IMAGE%) no
    else
        FAILURES=$((FAILURES+1))
    fi
}


###########################################################################
# function: update_x4_card <sernum>
#
update_x4_card()
{
    local SN=$1
    local FPGA FMC

    ##### Supply FPGA and FMC to update_x4_fpga_if_needed #####
    FPGA=$($UTA/sidekiq_probe -S $SN --fmtstring=%G)
    FMC=$($UTA/sidekiq_probe -S $SN --fmtstring=%M)

    update_x4_fpga_if_needed $SN $FPGA $FMC
}


###########################################################################
# function: update_x2_card <sernum>
#
update_x2_card()
{
    local SN=$1
    local FPGA

    ##### Check FPGA, only xcku060 supported for now #####
    FPGA=$($UTA/sidekiq_probe -S $SN --fmtstring=%G)

    case $FPGA in
        xcku060)
            FAIL=0
            update_golden_fpga_if_needed $SN $(readlink -f $x2_golden_fpga_image)

            # when FAIL is non-zero, card may not have golden FPGA, play it safe
            if [ $FAIL -eq 0 ]; then
                update_fpga_if_needed $SN $x2_fpga_date %X2_FPGA_VERSION_SPACE% $x2_fpga_hash $(my_readlink %X2_FPGA_IMAGE%) no
            else
                FAILURES=$((FAILURES+1))
            fi
            ;;

        xcku115)
            FAIL=0
            update_golden_fpga_if_needed $SN $(readlink -f $x2_htgk800_xcku115_golden_fpga_image)

            # when FAIL is non-zero, card may not have golden FPGA, play it safe
            if [ $FAIL -eq 0 ]; then
                update_fpga_if_needed $SN $x2_htgk800_xcku115_fpga_date %X2_FPGA_VERSION_SPACE% $x2_htgk800_xcku115_fpga_hash $(my_readlink %X2_HTGK800_XCKU115_FPGA_IMAGE%) no
            else
                FAILURES=$((FAILURES+1))
            fi
            ;;

        *)
            banner "!!! This Sidekiq Updater does not support the FPGA device ($FPGA) for Sidekiq Xs (s/n $SN)"
            ;;
    esac
}


###########################################################################
# function: update_m2_card <sernum>
#
update_m2_card()
{
    local SN=$1
    local REPROG

    # reference clock stamping to provide a valid setting
    banner "reference clock stamping on $SN"
    $UTA/ref_clock_stamp --serial $SN

    # firmware is accessible over USB so we can update even if drivers didn't load
    banner "checking firmware on $SN"

    # Capture the FX2 version currently running (greatly assumes the running version is the stored
    # version)
    local FWVER=$($UTA/sidekiq_probe -S $SN --fmtstring=%f)

    # update or consider updating the firmware only if the current version is no greater than what
    # we're offering
    if version_ge %M2_FW_VERSION% $FWVER; then
        # First load the firmware over USB into RAM, this helps in situations where the firmware is
        # corrupted since the version loaded into RAM will be pristine.
        $UTA/fx2_reprogram -s $SN -r --m2=$(my_readlink %M2_FW_IMAGE%) -m USB

        # see if the existing firmware verifies before attempting to program a new firmware or
        # overwriting a corrupt firmware
        if version_eq %M2_FW_VERSION% $FWVER; then
            $UTA/fx2_reprogram -s $SN -v --m2=$(my_readlink %M2_FW_IMAGE%)
            if [ $? -ne 0 ]; then
                # program and/or overwrite the firmware
                $UTA/fx2_reprogram -s $SN -e -v --m2=$(my_readlink %M2_FW_IMAGE%)
            fi
        else
            # upgrade the firmware
            $UTA/fx2_reprogram -s $SN -e -v --m2=$(my_readlink %M2_FW_IMAGE%)
        fi
    fi

    #
    # Check to see if the USB transport is available to know if REPROG is possible
    if $UTA/test_hardware_vers -S $SN -m USB >/dev/null 2>&1; then
        REPROG=yes
    else
        REPROG=no
    fi

    FAIL=0
    update_golden_fpga_if_needed $SN $(my_readlink $m2_golden_fpga_image)

    # when FAIL is non-zero, card may not have golden FPGA, play it safe
    if [ $FAIL -eq 0 ]; then
        update_fpga_if_needed $SN $m2_fpga_date %M2_FPGA_VERSION_SPACE% $m2_fpga_hash $(my_readlink %M2_FPGA_IMAGE%) ${REPROG}
    else
        FAILURES=$((FAILURES+1))
    fi
}


###########################################################################
# function: update_mpcie_card <sn>
#
update_mpcie_card()
{
    local SN=$1
    local REPROG

    # reference clock stamping to provide a valid setting
    banner "reference clock stamping on $SN"
    $UTA/ref_clock_stamp --serial $SN

    # firmware is accessible over USB so we can update even if drivers didn't load
    banner "checking firmware on $SN"

    # Capture the FX2 version currently running (greatly assumes the running version is the stored
    # version)
    local FWVER=$($UTA/sidekiq_probe -S $SN --fmtstring=%f)

    # update or consider updating the firmware only if the current version is no greater than what
    # we're offering
    if version_ge %MPCIE_FW_VERSION% $FWVER; then
        # First load the firmware over USB into RAM, this helps in situations where the firmware is
        # corrupted since the version loaded into RAM will be pristine.
        $UTA/fx2_reprogram -s $SN -r --mpcie=$(my_readlink %MPCIE_FW_IMAGE%) -m USB

        # see if the existing firmware verifies before attempting to program a new firmware or
        # overwriting a corrupt firmware
        if version_eq %MPCIE_FW_VERSION% $FWVER; then
            $UTA/fx2_reprogram -s $SN -v --mpcie=$(my_readlink %MPCIE_FW_IMAGE%)
            if [ $? -ne 0 ]; then
                # program and/or overwrite the firmware
                $UTA/fx2_reprogram -s $SN -e -v --mpcie=$(my_readlink %MPCIE_FW_IMAGE%)
            fi
        else
            # upgrade the firmware
            $UTA/fx2_reprogram -s $SN -e -v --mpcie=$(my_readlink %MPCIE_FW_IMAGE%)
        fi
    fi

    #
    # Check to see if the USB transport is available to know if REPROG is possible
    if $UTA/test_hardware_vers -S $SN -m USB >/dev/null 2>&1; then
        REPROG=yes
    else
        REPROG=no
    fi

    FAIL=0
    update_golden_fpga_if_needed $SN $(my_readlink $mpcie_golden_fpga_image)

    # when FAIL is non-zero, card may not have golden FPGA, play it safe
    if [ $FAIL -eq 0 ]; then
        update_fpga_if_needed $SN $mpcie_fpga_date %MPCIE_FPGA_VERSION_SPACE% $mpcie_fpga_hash $(my_readlink %MPCIE_FPGA_IMAGE%) ${REPROG}
    else
        FAILURES=$((FAILURES+1))
    fi
}


###########################################################################
# function: kill_skiq_app <APP> [NAME]
#
kill_skiq_app()
{
    local APP=$1
    local NAME=$2

    # default NAME to APP if not set
    NAME=${NAME:-$APP}

    pgrep $APP >/dev/null 2>&1
    if [ $? -eq 0 ]; then
        if [ -f /etc/init.d/$APP ]; then
            echo "--> Stopping $NAME service" >&2
            /etc/init.d/$APP stop
        else
            echo "--> Sending SIGINT to $NAME" >&2
            pkill -INT $APP
        fi

        #
        # try to wait here for instances to exit cleanly
        #
        echo -n "--> waiting for $NAME instances to exit " >&2
        timeout=60
        while [ $timeout -gt 0 ]; do
            pgrep $APP >/dev/null 2>&1
            if [ $? -ne 0 ]; then
                echo ' [ok]' >&2
                success=yes
                break
            else
                echo -n "." >&2
            fi
            sleep 1
            timeout=$((timeout - 1))
        done
        if [ x"$success" != x"yes" ]; then
            echo ' [fail]' >&2
            # instances are still hanging around, try harder
            echo "--> instances of $NAME still hanging around, proceed to hard kill" >&2
            pkill -TERM $APP
            #
            # try to wait here for instances to exit from hard kill
            #
            echo -n "--> waiting for $NAME instances to exit from hard kill " >&2
            timeout=60
            while [ $timeout -gt 0 ]; do
                pgrep $APP >/dev/null 2>&1
                if [ $? -ne 0 ]; then
                    echo ' [ok]' >&2
                    success=yes
                    break
                else
                    echo -n "." >&2
                fi
                sleep 1
                timeout=$((timeout - 1))
            done
            if [ x"$success" != x"yes" ]; then
                echo ' [fail]' >&2
                echo "failed to fully kill $NAME instances, aborting update..." >&2
                exit 1
            fi
        fi
    fi
}
###########################################################################


###########################################################################
# first things first, check for privileged user, the run-time installer will
# need elevated privileges to install /etc/init.d/sidekiq and others
#
if [ "$EUID" -ne 0 ]; then
    echo >&2
    echo -e "\tSidekiq Hardware Updater must be run as root (or using sudo) in order to remove / insert kernel modules and use USB without udev rules" >&2
    echo >&2
    display_help
    exit 1
fi


###########################################################################
# check for at least one argument
if [ $# -eq 0 ]; then
    display_help
    exit 1
fi


###########################################################################
# determine machine type
case $(uname -m) in
    arm*)
        case $(uname -r) in
            4.4.0)
                BC=arm_cortex-a9.gcc5.2_glibc_openwrt
                ;;
            3.14.16)
                echo "!!! This Sidekiq Runtime Installer no longer supports kernel version '$(uname -r)' for arch '$(uname -m)'" >&2
                echo "!!! If this is a Matchstiq device, please visit the support forum for update instructions" >&2
                exit 1
                ;;
            *)
                echo "!!! This Sidekiq updater does not support kernel version '$(uname -r)' for arch '$(uname -m)'" >&2
                exit 1
                ;;
        esac
        ;;
    aarch64)
        BC=aarch64.gcc6.3
        ;;
    x86_64)
        BC=x86_64.gcc
        ;;
    *)
        echo "!!! This Sidekiq updater does not support architecture '$(uname -m)'" >&2
        exit 1
        ;;
esac
echo "--> Detected build configuration: $BC" >&2
mkdir -p test_apps
tar -C test_apps -xf update_test_apps_$BC.tar.xz
UTA=./test_apps
FAILURES=0
###########################################################################


###########################################################################
# kill applications that could be using Sidekiq
#
kill_skiq_app srfs_rx SRFS
kill_skiq_app skylight Skylight
for app in floodlight spotlight era-controller fscdc-server; do
    kill_skiq_app $app
done
if [ -x /usr/bin/stop_everything ]; then
    echo "Stopping running killswitch services ..." >&2
    /usr/bin/stop_everything >> /tmp/epiq_stop_everything_log.txt 2>&1
fi
###########################################################################

###########################################################################
# loop over arguments to see if user wants to skip-driver-reload
#
SKIP_DRIVER_RELOAD=no
for arg; do
    case $(echo $arg | tr 'A-Z' 'a-z') in
        skip-driver-reload)
            SKIP_DRIVER_RELOAD=yes
            ;;
        *)
            ;;
    esac
done

###########################################################################
# try to install the proper kernel modules
#
if [ x"$SKIP_DRIVER_RELOAD" = x"no" ]; then
    # the Jetson platforms are special so determine if we're on a Jetson
    # platform and then load drivers based on MODEL
    MODEL="unknown"
    if [ -e /proc/device-tree/compatible ]; then
        COMPATIBLE=$(strings /proc/device-tree/compatible | grep -m1 -E '(nvidia,tegra186|nvidia,tegra210|nvidia,jetson-xavier|nvidia,tegra194)')
        case "$COMPATIBLE" in
            nvidia,tegra210)
                MODEL="jetson-tx1"
                ;;
            nvidia,tegra186)
                MODEL="jetson-tx2"
                ;;
            nvidia,jetson-xavier|nvidia,tegra194)
                MODEL="jetson-xavier"
                ;;
        esac
    fi
    case "$BC" in
        arm_cortex-a9.gcc5.2_glibc_openwrt)
            MODEL="openwrt"
            ;;
    esac
    case "$MODEL" in
        jetson-tx1|jetson-tx2|jetson-xavier)
            sh ./driver/load_sidekiq_drivers.sh ./driver/$MODEL
            ;;
        openwrt)
            sh ./driver/load_sidekiq_drivers.sh ./driver
            ;;
        *)
            tar -C driver -xvf drivers.tar.xz ./$(uname -r)/ 2>/dev/null
            if [ $? -eq 0 ]; then
                sh ./driver/load_sidekiq_drivers.sh ./driver
            else
                echo "Kernel modules with version $(uname -r) not found in updater! Proceeding with loaded kernel modules or using USB" >&2
            fi
            ;;
    esac
    echo "waiting for kernel modules to detect and settle" >&2
    sleep 5
fi

###########################################################################
# run card manager uninit to clear out-of-date shm if possible
#
$UTA/card_mgr_uninit >/dev/null 2>&1

###########################################################################
# loop over arguments to determine if all specified cards are available
#
# special case if 'all' is specified
#
for arg; do
    case $(echo $arg | tr 'A-Z' 'a-z') in
        all)
            SN=$($UTA/sidekiq_probe)
            [ -z "$SN" ] && echo "Warning: no Sidekiq cards found" && exit 0
            ;;
        *)
            ;;
    esac
done

unset SKIQMAP
[ -z "$SN" ] && SN="$@"
for sn in $SN; do
    banner "checking for serial number $sn" >&2
    FF=$($UTA/sidekiq_probe --serial $sn --fmtstring=%P 2>/dev/null)
    if [ $? -eq 0 ]; then
        case $FF in
            NV100)
                echo "--> Detected card $sn as an NV100"
                SKIQMAP="$SKIQMAP $sn=nv100"
                ;;
            M.2-2280)
                echo "--> Detected card $sn as an M.2-2280"
                SKIQMAP="$SKIQMAP $sn=m.2-2280"
                ;;
            X4)
                echo "--> Detected card $sn as an X4"
                SKIQMAP="$SKIQMAP $sn=x4"
                ;;
            X2)
                echo "--> Detected card $sn as an X2"
                SKIQMAP="$SKIQMAP $sn=x2"
                ;;
            m.2)
                echo "--> Detected card $sn as an m.2"
                SKIQMAP="$SKIQMAP $sn=m.2"
                ;;
            mPCIe)
                echo "--> Detected card $sn as an mPCIe"
                SKIQMAP="$SKIQMAP $sn=mpcie"
                ;;
            Z2)
                echo "--> Detected card $sn as an Z2"
                SKIQMAP="$SKIQMAP $sn=z2"
                ;;
            Z3U)
                echo "--> Detected card $sn as an Z3u"
                SKIQMAP="$SKIQMAP $sn=z3u"
                ;;
            *)
                echo "!!! Did not detect card with serial number $sn"
                exit 1
                ;;
        esac
    else
        echo "!!! Did not detect card with serial number $sn"
        exit 1
    fi
done


###########################################################################
# at this point, all serial numbers have been verified
###########################################################################


###########################################################################
# iterate over the serial numbers and apply the update
for pair in ${SKIQMAP}; do
    sn=$(echo $pair | cut -d= -f1);
    typ=$(echo $pair | cut -d= -f2);
    case $typ in
        nv100)
            update_nv100_card $sn
            ;;
        m.2-2280)
            update_m2_2280_card $sn
            ;;
        x4)
            update_x4_card $sn
            ;;
        x2)
            update_x2_card $sn
            ;;
        m.2)
            update_m2_card $sn
            ;;
        mpcie)
            update_mpcie_card $sn
            ;;
        z2)
            echo "--> Nothing to update on a Sidekiq Z2"
            ;;
        z3u)
            echo "--> Nothing to update on a Sidekiq Z3u"
            ;;
        *)
            echo "!!! unexpected form-factor in list: '$typ'"
            ;;
    esac
done

if [ $FAILURES -eq 0 ]; then
    banner "Sidekiq Hardware update completed"
elif [ $FAILURES -eq 1 ]; then
    banner "Sidekiq Hardware update completed (with 1 failure)"
else
    banner "Sidekiq Hardware update completed (with $FAILURES failures)"
fi

###########################################################################
# run card manager uninit to clear out-of-date shm if possible
#
$UTA/card_mgr_uninit >/dev/null 2>&1
