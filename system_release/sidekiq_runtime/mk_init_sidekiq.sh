rm -f /etc/init.d/sidekiq

cp sidekiq.template sidekiq
chmod 755 sidekiq

sed -i -e "s,SIDEKIQ_HOME,$1,g" sidekiq
