#
# Copyright 2017 Epiq Solutions, All Rights Reserved
#
_sidekiq()
{
    local cur prev opts i prev_opt
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"

    local bn=$(basename "${COMP_WORDS[0]}")
    case "$bn" in
        prog_fpga)
            opts="-h --help -c --card= -s --source= --flash"
            ;;
        read_accel)
            opts="-h --help -c --card= --repeat="
            ;;
        read_temp)
            opts="-h --help -c --card="
            ;;
        rx_benchmark)
            opts="-h --help -c --card= --handle= -r --rate= --target= --threshold= -t --time= --blocking"
            ;;
        rx_samples)
            opts="-h --help -b --bandwidth= -c --card= -d --destination= -f --frequency= -g --gain= --warp= --handle= -r --rate= -w --words= --counter --meta --packed --blocking"
            ;;
        store_user_fpga)
            opts="-h --help -c --card= -S --serial= -s --source= --reload -v --verbose --verify"
            ;;
        sweep_receive)
            opts="-h --help --blocks= -c --card= -r --rate= --repeat= --start= --stop= --step= --blocking"
            ;;
        test_golden_present)
            opts="-h --help -c --card= -s --serial="
            ;;
        tx_benchmark)
            opts="-h --help --block-size= -c --card= -r --rate= --target= --threads= --threshold= -t --time= --priority="
            ;;
        tx_samples)
            opts="-h --help -a --attenuation= -b --bandwidth= --block-size= -c --card= -f --frequency= --handle= -r --rate= --repeat= -s --source= -t --timestamp= --immediate --packed"
            ;;
        version_test)
            opts="-h --help -c --card= --filters -s --source="
            ;;
        xcv_benchmark)
            opts="-h --help --block-size= -c --card= -r --rate= --target= --threads= --threshold= -t --time= --blocking"
            ;;
        *)
            return 0
            ;;
    esac

    # remove previously instantiated options (but not their short/long counterparts)
    for (( i=1; i < COMP_CWORD; i++ )); do
        prev_opt="${COMP_WORDS[i]}"
        opts=$(echo "$opts" | tr ' ' '\n' | grep -v "^$prev_opt=\?\$")
    done

    # TODO - how to remove both short and long options if they've been used

    echo "cur=${cur} prev=${prev}" > debug

    if [[ ${prev} == = ]]; then
        prev=${COMP_WORDS[COMP_CWORD-2]}
    fi
    
    # handle options that have arguments.  the '=' has been removed here
    # intentionally to make the argument list completion work.
    case "${prev}" in
        --block-size)
            COMPREPLY=( $(compgen -W "252 508 1020 2044 4092 8188 16380 32764 65532" -- ${cur#=}) )
            return 0
            ;;
        -c|--card)
            COMPREPLY=( $(compgen -W "$(seq 0 3)" -- ${cur#=}) )
            return 0
            ;;
        --handle)
            COMPREPLY=( $(compgen -W "a1 a2 b1 both all" -- ${cur#=}) )
            return 0
            ;;
        -m|--mode)
            COMPREPLY=( $(compgen -W "pcie usb" -- ${cur#=}) )
            return 0
            ;;
        -s|--source)
            COMPREPLY=( $(compgen -G '*.@(a1|a2|b1|bin|dat)' -- ${cur#=}) )
            return 0
            ;;
        -d|--destination)
            COMPREPLY=( $(compgen -G '*.@(a1|a2|b1|bin|dat)' -- ${cur#=} | sed -e 's/\.a[12]$//;s/\.bin$//;s/\.dat$//') )
            return 0
            ;;
    esac

    COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    [[ $COMPREPLY == *= ]] && compopt -o nospace
    
    return 0
}

complete -F _sidekiq prog_fpga read_accel read_temp rx_benchmark rx_samples store_user_fpga sweep_receive test_golden_present tx_benchmark tx_samples version_test xcv_benchmark
