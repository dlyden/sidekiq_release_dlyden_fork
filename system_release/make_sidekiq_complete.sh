#! /bin/bash

APP=$(readlink -f "$0")
APP=$(basename "$APP")
if [ $# -ne 2 ]; then
    echo "usage: $APP <YYYYMMDD> <X.Y.Z>" >&2
    echo "       where YYYYMMDD is the image version" >&2
    echo "       where X.Y.Z is the SDK version" >&2
    exit 1
fi

IMAGE=$1
SDK=$2
SDK_RPM=$(echo $SDK | tr '-' '_') # translates - to _ because RPMs enforce that in their names
DEST=sidekiq_complete
POST_EXTRACT=sidekiq_complete_x86.sh
POST_EXTRACT_OPENWRT=sidekiq_complete_openwrt.sh
POST_EXTRACT_JETSON=sidekiq_complete_jetson.sh
POST_EXTRACT_Z3U=sidekiq_complete_z3u.sh
DT=$(date -d $IMAGE '+%B %d, %Y')

# exit on error and errors with pipelines
set -o errexit
set -o pipefail

function copy-file-here()
{
    local FN=$1

    if [ ! -e $FN ]; then
        echo " - required file '$FN' is missing, quitting" >&2
        exit 1
    fi

    echo "copying required file '$FN'" >&2
    mkdir -p $DEST
    cp $FN $DEST/
}

function strip-script()
{
    local FN=$1
    local MKT=$(mktemp)

    # transmogrify the script (strip out whitespace / comments)
    # http://stackoverflow.com/questions/25291228/how-to-remove-comments-from-a-bash-script
    env "BASH_FUNC_tmp_%%=() {
          $(<$FN)
         }" bash -c 'declare -f tmp_' | \
    tail -n+3 | \
    head -n-1 | \
    cut -b 5- > $MKT

    (echo "#! /bin/bash"; cat $MKT) > $FN
    rm -f $MKT
}

function remove-previous-files()
{
    echo "removing old SDK bundle, Sidekiq HW updater, and run-time installer"
    rm -f $DEST/sidekiq_hardware_updater_for_v*.sh \
        $DEST/sidekiq_*_imx6.opk \
        $DEST/sidekiq_sdk_v*.tar.xz \
        $DEST/install_sidekiq_runtime_v*_*.sh \
        $DEST/$POST_EXTRACT_JETSON \
        $DEST/$POST_EXTRACT_OPENWRT \
        $DEST/$POST_EXTRACT_Z3U \
        $DEST/$POST_EXTRACT
}

function makeself-exec()
{
    local OUT=$1
    local DESC=$2
    local PE=$3

    XZ_OPT=-9e fakeroot makeself \
      --header ./makeself-header.sh \
      --nox11 --xz $DEST $OUT "$DESC" "$PE"
}

###########################################################################
#
# create system updater for x86
#
###########################################################################
remove-previous-files

# copy SDK, image, and run-time library package
copy-file-here ../sdk_release/sidekiq_sdk_v${SDK}.tar.xz
copy-file-here sidekiq_hardware_updater_for_v${SDK}.sh
copy-file-here install_sidekiq_runtime_v${SDK}_${IMAGE}_x86.sh

# order is important here so variables that are substrings of other variables
# don't get clobbered
cp -f post_extract/$POST_EXTRACT.template $DEST/$POST_EXTRACT
sed -i -e "s,SDK_VERSION_RPM,$SDK_RPM,g" $DEST/$POST_EXTRACT
sed -i -e "s,SDK_VERSION,$SDK,g" $DEST/$POST_EXTRACT
sed -i -e "s,IMAGE_VERSION,$IMAGE,g" $DEST/$POST_EXTRACT

strip-script $DEST/$POST_EXTRACT
chmod a+x $DEST/$POST_EXTRACT
makeself-exec install_sidekiq_complete_for_v${SDK}_${IMAGE}_x86.sh \
              "Sidekiq Complete Installation (v${SDK}) from ${DT} for x86" \
              ./$POST_EXTRACT

###########################################################################
#
# create system updater for Jetson TX1/TX2/Xavier
#
###########################################################################
remove-previous-files

# copy SDK, image, and run-time library package
copy-file-here ../sdk_release/sidekiq_sdk_v${SDK}_jetson.tar.xz
copy-file-here sidekiq_hardware_updater_for_v${SDK}.sh
copy-file-here install_sidekiq_runtime_v${SDK}_${IMAGE}_jetson.sh

# order is important here so variables that are substrings of other variables
# don't get clobbered
cp -f post_extract/$POST_EXTRACT_JETSON.template $DEST/$POST_EXTRACT_JETSON
sed -i -e "s,SDK_VERSION_RPM,$SDK_RPM,g" $DEST/$POST_EXTRACT_JETSON
sed -i -e "s,SDK_VERSION,$SDK,g" $DEST/$POST_EXTRACT_JETSON
sed -i -e "s,IMAGE_VERSION,$IMAGE,g" $DEST/$POST_EXTRACT_JETSON

strip-script $DEST/$POST_EXTRACT_JETSON
chmod a+x $DEST/$POST_EXTRACT_JETSON
makeself-exec install_sidekiq_complete_for_v${SDK}_${IMAGE}_jetson.sh \
              "Sidekiq Complete Installation (v${SDK}) from ${DT} for NVIDIA Jetson TX1/TX2/Xavier" \
              ./$POST_EXTRACT_JETSON

###########################################################################
#
# create system updater for Z3u
#
###########################################################################
remove-previous-files

# copy SDK, image, and run-time library package
copy-file-here ../sdk_release/sidekiq_sdk_v${SDK}_z3u.tar.xz
copy-file-here sidekiq_hardware_updater_for_v${SDK}.sh
copy-file-here install_sidekiq_runtime_v${SDK}_${IMAGE}_z3u.sh

# order is important here so variables that are substrings of other variables
# don't get clobbered
cp -f post_extract/$POST_EXTRACT_Z3U.template $DEST/$POST_EXTRACT_Z3U
sed -i -e "s,SDK_VERSION_RPM,$SDK_RPM,g" $DEST/$POST_EXTRACT_Z3U
sed -i -e "s,SDK_VERSION,$SDK,g" $DEST/$POST_EXTRACT_Z3U
sed -i -e "s,IMAGE_VERSION,$IMAGE,g" $DEST/$POST_EXTRACT_Z3U

strip-script $DEST/$POST_EXTRACT_Z3U
chmod a+x $DEST/$POST_EXTRACT_Z3U
makeself-exec install_sidekiq_complete_for_v${SDK}_${IMAGE}_z3u.sh \
              "Sidekiq Complete Installation (v${SDK}) from ${DT} for Z3u" \
              ./$POST_EXTRACT_Z3U
