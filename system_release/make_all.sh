#! /bin/bash

if [ $# -ne 2 ]; then
    echo "usage: $(basename $(readlink -f $0)) <YYYYMMDD> <X.Y.Z>" >&2
    echo "       where YYYYMMDD is the image version" >&2
    echo "       where X.Y.Z is the SDK version" >&2
    exit 1
fi

function perform()
{
    echo "--> $@" >&2
    "$@"
}

D=$(dirname $(readlink -f $0))
IMAGE=$1
SDK=$2

perform $D/make_sidekiq_hardware_updater.sh $IMAGE $SDK
perform $D/make_sidekiq_runtime.sh $IMAGE $SDK

# make_sidekiq_complete.sh depends on a sidekiq_hardware_updater.sh and sidekiq_runtime.sh
perform $D/make_sidekiq_complete.sh $IMAGE $SDK
